package ru.mirea.hw1.task1;

import java.util.List;

public class Square56 {

    public static List<Integer> square56(List<Integer> inputList) {
        return inputList
                .stream()
                .map(x -> x * x + 10)
                .filter(x -> x % 10 != 5 && x % 10 != 6)
                .toList();
    }
}
