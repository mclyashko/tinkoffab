package ru.mirea.hw1.task1;

import java.util.List;

import static ru.mirea.hw1.task1.Square56.square56;

public class Tests {
    public static void runTests() {
        List<List<Integer>> input = List.of(
                List.of(3, 1, 4),
                List.of(1),
                List.of(2),
                List.of(),
                List.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        );
        List<List<Integer>> output = List.of(
                List.of(19, 11),
                List.of(11),
                List.of(14),
                List.of(),
                List.of(10, 11, 14, 19, 59, 74, 91, 110)
        );

        for (int i = 0; i < input.size(); i++) {
            List<Integer> testIn = input.get(i);
            List<Integer> testOut = output.get(i);

            List<Integer> methodOut = square56(testIn);

            if (testOut.equals(methodOut)) {
                System.out.printf("test №%d passed\n", i);
            } else {
                System.out.printf("test №%d NOT passed\n", i);
            }
        }
    }
}
