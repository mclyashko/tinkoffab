package ru.mirea.hw1.task2;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CountMap {
    public static Map<Integer, Integer> duplicateCounter(List<Integer> inputList) {
        return inputList
                .stream()
                .collect(Collectors.toMap(
                        k -> k,
                        v -> 1,
                        Integer::sum
                ))
                .entrySet()
                .stream()
                .filter(e -> e.getValue() >= 2)
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue
                ));
    }
}
