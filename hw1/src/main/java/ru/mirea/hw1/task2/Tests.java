package ru.mirea.hw1.task2;

import java.util.List;
import java.util.Map;

import static java.util.Map.entry;
import static ru.mirea.hw1.task2.CountMap.duplicateCounter;

public class Tests {
    public static void runTests() {
        List<List<Integer>> input = List.of(
                List.of(1, 2, 3, 3, 3),
                List.of(1, 2, 3),
                List.of(),
                List.of(20, 83, 20, 3, 79, 53, 20, 3, 46, 20, 20, 40, 42, 65, 79, 67, 18, 13, 20, 87, 79),
                List.of(100, 100, 100, 100, 100)
        );
        List<Map<Integer, Integer>> output = List.of(
                Map.ofEntries(
                        entry(3, 3)
                ),
                Map.ofEntries(),
                Map.ofEntries(),
                Map.ofEntries(
                        entry(3, 2),
                        entry(79, 3),
                        entry(20, 6)
                ),
                Map.ofEntries(
                        entry(100, 5)
                )
        );

        for (int i = 0; i < input.size(); i++) {
            List<Integer> testIn = input.get(i);
            Map<Integer, Integer> testOut = output.get(i);

            Map<Integer, Integer> methodOut = duplicateCounter(testIn);

            if (testOut.equals(methodOut)) {
                System.out.printf("test №%d passed\n", i);
            } else {
                System.out.printf("test №%d NOT passed\n", i);
            }
        }
    }
}
