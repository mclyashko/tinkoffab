package ru.mirea.hw1.task3;

public class Starter {
    public static void syncPrint() {
        Syncer sync = new Syncer(1, 3);

        Thread thread1 = new Thread(new Worker(1, 6, sync));
        Thread thread2 = new Thread(new Worker(2, 6, sync));
        Thread thread3 = new Thread(new Worker(3, 6, sync));

        thread1.start();
        thread2.start();
        thread3.start();

        try {
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
