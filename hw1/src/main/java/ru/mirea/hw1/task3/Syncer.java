package ru.mirea.hw1.task3;

public class Syncer {
    private final int maxWorkerId;
    private final int minWorkerId;
    private int nowWorkerId;
    private final Object locker = new Object();

    @SuppressWarnings("SameParameterValue")
    Syncer(int minWorkerId, int maxWorkerId) {
        this.minWorkerId = minWorkerId;
        this.nowWorkerId = minWorkerId;
        this.maxWorkerId = maxWorkerId;
    }

    // Синхронизация потоков на основе их id
    public void syncedPrint(int id) {
        synchronized (locker) {
            while (id != nowWorkerId) {
                try {
                    locker.wait();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            System.out.print(nowWorkerId);
            nowWorkerId++;
            if (nowWorkerId > maxWorkerId) {
                nowWorkerId = minWorkerId;
            }
            locker.notifyAll();
        }
    }
}
