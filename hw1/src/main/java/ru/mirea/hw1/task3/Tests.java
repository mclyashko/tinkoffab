package ru.mirea.hw1.task3;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Objects;

import static ru.mirea.hw1.task3.Starter.syncPrint;

public class Tests {
    private static final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private static final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private static final PrintStream modifiedOut = new PrintStream(outContent);
    private static final PrintStream modifiedErr = new PrintStream(errContent);
    private static final PrintStream originalOut = System.out;
    private static final PrintStream originalErr = System.err;

    // Заменить потоки стандартного вывода пользовательскими
    private static void setUpStreams() {
        System.setOut(modifiedOut);
        System.setErr(modifiedErr);
    }

    // Вернуть потоки стандартного вывода на стандартные
    private static void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    public static void runTests() {
        setUpStreams();

        syncPrint();

        String output = outContent.toString();

        restoreStreams();

        if (!Objects.equals(output, "123123123123123123")) {
            System.out.println("test NOT passed");
        } else {
            System.out.println("test passed");
        }
    }
}
