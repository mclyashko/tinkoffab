package ru.mirea.hw1.task3;

public class Worker implements Runnable {
    private final int id;
    private final int amount;
    private final Syncer printSyncer;

    @SuppressWarnings("SameParameterValue")
    Worker(int initId, int initAmount, Syncer printSyncer) {
        this.id = initId;
        this.amount = initAmount;
        this.printSyncer = printSyncer;
    }

    @Override
    public void run() {
        for (int i = 0; i < amount; i++) {
            printSyncer.syncedPrint(id);
        }
    }
}
