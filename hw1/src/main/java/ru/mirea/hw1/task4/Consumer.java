package ru.mirea.hw1.task4;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable{
    private final BlockingQueue<Integer> inputCommunicativeQueue;
    Consumer(BlockingQueue<Integer> inputCommunicativeQueue) {
        this.inputCommunicativeQueue = inputCommunicativeQueue;
    }

    @Override
    public void run() {
        while (true) {
            int input;

            try {
                input = inputCommunicativeQueue.take();
            } catch (InterruptedException e) {
                return;
            }

            System.out.printf("%d ", input + 10);
        }
    }
}
