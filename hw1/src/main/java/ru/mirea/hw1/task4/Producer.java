package ru.mirea.hw1.task4;

import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable {
    private final BlockingQueue<Integer> inputStorageQueue;
    private final BlockingQueue<Integer> outputCommunicativeQueue;
    Producer(BlockingQueue<Integer> inputStorageQueue, BlockingQueue<Integer> outputCommunicativeQueue) {
        this.inputStorageQueue = inputStorageQueue;
        this.outputCommunicativeQueue = outputCommunicativeQueue;
    }

    @Override
    public void run() {
        while (true) {
            int input;

            try {
                input = inputStorageQueue.take();
            } catch (InterruptedException e) {
                return;
            }

            try {
                outputCommunicativeQueue.put(input * 2);
            } catch (InterruptedException e) {
                return;
            }
        }
    }
}
