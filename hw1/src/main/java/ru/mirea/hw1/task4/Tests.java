package ru.mirea.hw1.task4;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.stream.Collectors;

public class Tests {
    private static final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private static final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private static final PrintStream modifiedOut = new PrintStream(outContent);
    private static final PrintStream modifiedErr = new PrintStream(errContent);
    private static final PrintStream originalOut = System.out;
    private static final PrintStream originalErr = System.err;

    // Заменить потоки стандартного вывода пользовательскими
    private static void setUpStreams() {
        System.setOut(modifiedOut);
        System.setErr(modifiedErr);
    }

    // Вернуть потоки стандартного вывода на стандартные
    private static void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }
    public static void runTests() {
        setUpStreams();

        Map<Integer, Integer> testMap = new HashMap<>();
        BlockingQueue<Integer> storageQueue = new ArrayBlockingQueue<>(10);
        for (int i = 0; i < 10; i++) {
            try {
                storageQueue.put(i);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            if (testMap.containsKey(i)) {
                testMap.put(i * 2 + 10, testMap.get(i) + 1);
            } else {
                testMap.put(i * 2 + 10, 1);
            }
        }
        BlockingQueue<Integer> communicativeQueue = new ArrayBlockingQueue<>(3);

        Thread producer2Mul1 = new Thread(new Producer(storageQueue, communicativeQueue));
        Thread producer2Mul2 = new Thread(new Producer(storageQueue, communicativeQueue));
        Thread consumer10Add1 = new Thread(new Consumer(communicativeQueue));
        Thread consumer10Add2 = new Thread(new Consumer(communicativeQueue));
        Thread consumer10Add3 = new Thread(new Consumer(communicativeQueue));

        producer2Mul1.start();
        producer2Mul2.start();
        consumer10Add1.start();
        consumer10Add2.start();
        consumer10Add3.start();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        producer2Mul1.interrupt();
        producer2Mul2.interrupt();
        consumer10Add1.interrupt();
        consumer10Add2.interrupt();
        consumer10Add3.interrupt();

        String output = outContent.toString();

        restoreStreams();

        Map<Integer, Integer> methodMap = Arrays.stream(output.split(" ")).map(Integer::parseInt).collect(Collectors.toMap(
                k -> k,
                v -> 1,
                Integer::sum
        ));

        if (methodMap.equals(testMap)) {
            System.out.println("test passed");
        } else {
            System.out.println("test NOT passed");
        }
    }
}
