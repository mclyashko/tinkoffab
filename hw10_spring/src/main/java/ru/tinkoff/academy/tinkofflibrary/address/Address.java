package ru.tinkoff.academy.tinkofflibrary.address;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.tinkoff.academy.tinkofflibrary.publishing.Publishing;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@Table(name = "ADDRESS", schema = "LIBRARY")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, updatable = false)
    private Long id;

    @Column(name = "COUNTRY")
    private String country;
    @Column(name = "CITY")
    private String city;
    @Column(name = "STREET")
    private String street;
    @Column(name = "HOUSE")
    private String house;

    @OneToOne
    @JoinColumn(name = "PUBLISHING_ID")
    @JsonIgnoreProperties("address")
    private Publishing publishing;
}
