package ru.tinkoff.academy.tinkofflibrary.address;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AddressService {
    private final AddressRepository addressRepository;

    public Address saveAddress(Address address) {
        return addressRepository.save(address);
    }

    public List<Address> findAddresses() {
        return addressRepository.findAll();
    }

    public Optional<Address> findAddressById(Long addressId) {
        return addressRepository.findById(addressId);
    }

    public Address updateAddress(Address address) {
        Address addressFromDb = addressRepository.getReferenceById(
            address.getId()
        );

        addressFromDb
            .setCountry(address.getCountry())
            .setCity(address.getCity())
            .setStreet(address.getStreet())
            .setHouse(address.getHouse())
            .setPublishing(address.getPublishing());

        return addressRepository.save(addressFromDb);
    }

    public void deleteAddressById(Long addressId) {
        addressRepository.deleteById(addressId);
    }
}
