package ru.tinkoff.academy.tinkofflibrary.author;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.tinkoff.academy.tinkofflibrary.book.Book;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@Table(name = "AUTHOR", schema = "LIBRARY")
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, updatable = false)
    private Long id;

    private String name;

    @ManyToMany(fetch = FetchType.EAGER,
        targetEntity = Book.class)
    @JoinTable(name = "BOOK_MAPPING_AUTHOR",
        schema = "LIBRARY",
        joinColumns = {@JoinColumn(name = "AUTHOR_MAPPING_ID", nullable = false)},
        inverseJoinColumns = {@JoinColumn(name = "BOOK_MAPPING_ID", nullable = false)})
    @JsonIgnoreProperties("authors")
    private Set<Book> books;
}
