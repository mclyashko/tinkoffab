package ru.tinkoff.academy.tinkofflibrary.author;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthorService {

    private final AuthorRepository authorRepository;

    public Author saveAuthor(Author author) {
        return authorRepository.save(author);
    }

    public List<Author> findAuthors() {
        return authorRepository.findAll();
    }

    public Optional<Author> findAuthorById(Long authorId) {
        return authorRepository.findById(authorId);
    }

    public Author updateAuthor(Author author) {
        Author authorFromDb = authorRepository.getReferenceById(
            author.getId()
        );

        authorFromDb
            .setName(author.getName())
            .setBooks(author.getBooks());

        return authorRepository.save(authorFromDb);
    }

    public void deleteAuthorById(Long authorId) {
        authorRepository.deleteById(authorId);
    }
}
