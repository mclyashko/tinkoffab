package ru.tinkoff.academy.tinkofflibrary.book;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.time.LocalDate;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.domain.Sort;
import ru.tinkoff.academy.tinkofflibrary.author.Author;
import ru.tinkoff.academy.tinkofflibrary.genre.Genre;
import ru.tinkoff.academy.tinkofflibrary.publishing.Publishing;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@Table(name = "BOOK", schema = "LIBRARY")
public class Book {

    public static final String BOOK_ID_FIELD = "id";
    public static final String BOOK_TITLE_FIELD = "title";
    public static final String BOOK_PUBLICATION_DATE_FIELD = "publicationDate";
    public static final String BOOK_AUTHORS_FIELD = "authors";
    public static final String BOOK_GENRE_FIELD = "genre";
    public static final String BOOK_PUBLISHING_FIELD = "publishing";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, updatable = false)
    private Long id;

    @Column(name = "TITLE")
    private String title;
    @Column(name = "PUBLICATION_DATE")
    private LocalDate publicationDate;

    @ManyToMany(fetch = FetchType.EAGER,
        cascade =
            {
                CascadeType.DETACH,
                CascadeType.MERGE,
                CascadeType.REFRESH,
                CascadeType.PERSIST
            },
        targetEntity = Author.class)
    @JoinTable(name = "BOOK_MAPPING_AUTHOR",
        schema = "LIBRARY",
        joinColumns = {@JoinColumn(name = "BOOK_MAPPING_ID", nullable = false)},
        inverseJoinColumns = {@JoinColumn(name = "AUTHOR_MAPPING_ID", nullable = false)})
    @JsonIgnoreProperties("books")
    private Set<Author> authors;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "GENRE_ID")
    @JsonIgnoreProperties("books")
    private Genre genre;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PUBLISHING_ID")
    @JsonIgnoreProperties("books")
    private Publishing publishing;

    public static Sort makeSortByFieldNamed(String filedName) {
        return checkIfFiledExistsByName(filedName) ? Sort.by(filedName) : Sort.unsorted();
    }

    private static boolean checkIfFiledExistsByName(String filedName) {
        if (filedName == null) {
            return false;
        }

        try {
            Book.class.getDeclaredField(filedName);
            return true;
        } catch (NoSuchFieldException e) {
            return false;
        }
    }
}
