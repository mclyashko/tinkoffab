package ru.tinkoff.academy.tinkofflibrary.book;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/book")
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    @PostMapping
    public Book saveBook(@RequestBody Book book) {
        return bookService.saveBook(book);
    }

    @GetMapping
    public List<Book> findBooks() {
        return bookService.findBooks();
    }

    @GetMapping("/pageable")
    public List<Book> findBooksPageable(
        @RequestParam(name = "page") int page,
        @RequestParam(name = "size") int size,
        @RequestParam(name = "sortBy", required = false) String sortBy
    ) {
        Pageable pageable = PageRequest.of(--page, size, Book.makeSortByFieldNamed(sortBy));

        return bookService.findBooksPageable(pageable).getContent();
    }

    @GetMapping("/specification/title_like")
    public List<Book> findBooksSpecificationTitleLike(
        @RequestParam(name = "title_like") String titleLike
    ) {
        Specification<Book> titleLikeSpecification = Specification.where(
            BookSpecification.titleLike(titleLike));

        return bookService.findBooksSpecification(titleLikeSpecification);
    }

    @GetMapping("/specification_pageable/publication_date")
    public List<Book> findBooksSpecificationPublicationDateBetweenPageable(
        @RequestParam(name = "begin") LocalDate begin,
        @RequestParam(name = "end") LocalDate end,
        @RequestParam(name = "page") int page,
        @RequestParam(name = "size") int size,
        @RequestParam(name = "sortBy", required = false) String sortBy
    ) {
        Specification<Book> publicationDateBetweenSpecification = Specification.where(
            BookSpecification.publicationDateBetween(begin, end)
        );

        Pageable pageable = PageRequest.of(--page, size, Book.makeSortByFieldNamed(sortBy));

        return bookService.findBooksSpecificationPageable(
                publicationDateBetweenSpecification,
                pageable
            )
            .getContent();
    }


    @GetMapping("/{bookId}")
    public ResponseEntity<Book> findBookById(@PathVariable Long bookId) {
        Optional<Book> book = bookService.findBookById(bookId);

        if (book.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(book.get(), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<Book> updateBook(@RequestBody Book book) {
        try {
            Book updatedBook = bookService.updateBook(book);
            return new ResponseEntity<>(updatedBook, HttpStatus.OK);
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{bookId}")
    public void deleteBookById(@PathVariable Long bookId) {
        bookService.deleteBookById(bookId);
    }
}
