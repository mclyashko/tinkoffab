package ru.tinkoff.academy.tinkofflibrary.book;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BookService {

    private final BookRepository bookRepository;

    public Book saveBook(Book book) {
        return bookRepository.save(book);
    }

    public List<Book> findBooks() {
        return bookRepository.findAll();
    }

    public Page<Book> findBooksPageable(Pageable pageable) {
        return bookRepository.findAll(pageable);
    }

    public List<Book> findBooksSpecification(Specification<Book> specification) {
        return bookRepository.findAll(specification);
    }

    public Page<Book> findBooksSpecificationPageable(
        Specification<Book> specification,
        Pageable pageable
    ) {
        return bookRepository.findAll(specification, pageable);
    }

    public Optional<Book> findBookById(Long bookId) {
        return bookRepository.findById(bookId);
    }

    public Book updateBook(Book book) {
        Book bookFromDb = bookRepository.getReferenceById(
            book.getId()
        );

        bookFromDb
            .setTitle(book.getTitle())
            .setPublicationDate(book.getPublicationDate())
            .setAuthors(book.getAuthors())
            .setGenre(book.getGenre())
            .setPublishing(book.getPublishing());

        return bookRepository.save(bookFromDb);
    }

    public void deleteBookById(Long bookId) {
        bookRepository.deleteById(bookId);
    }
}
