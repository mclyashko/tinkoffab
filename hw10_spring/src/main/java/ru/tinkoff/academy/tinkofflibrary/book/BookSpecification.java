package ru.tinkoff.academy.tinkofflibrary.book;

import java.time.LocalDate;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component
public class BookSpecification {

    public static Specification<Book> titleLike(String pattern) {
        return ((root, criteriaQuery, criteriaBuilder) ->
            criteriaBuilder.like(root.get(Book.BOOK_TITLE_FIELD), pattern));
    }

    public static Specification<Book> publicationDateBetween(
        LocalDate begin,
        LocalDate end
    ) {
        return ((root, criteriaQuery, criteriaBuilder) ->
            criteriaBuilder.between(root.get(Book.BOOK_PUBLICATION_DATE_FIELD), begin, end));
    }
}
