package ru.tinkoff.academy.tinkofflibrary.genre;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GenreService {

    private final GenreRepository genreRepository;

    public Genre saveGenre(Genre genre) {
        return genreRepository.save(genre);
    }

    public List<Genre> findGenres() {
        return genreRepository.findAll();
    }

    public Optional<Genre> findGenreById(Long genreId) {
        return genreRepository.findById(genreId);
    }

    public Genre updateGenre(Genre genre) {
        Genre genreFromDb = genreRepository.getReferenceById(
            genre.getId()
        );

        genreFromDb
            .setName(genre.getName())
            .setDescription(genre.getDescription())
            .setBooks(genre.getBooks());

        return genreRepository.save(genreFromDb);
    }

    public void deleteGenreById(Long genreId) {
        genreRepository.deleteById(genreId);
    }
}
