package ru.tinkoff.academy.tinkofflibrary.publishing;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.tinkoff.academy.tinkofflibrary.address.Address;
import ru.tinkoff.academy.tinkofflibrary.book.Book;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@Table(name = "PUBLISHING", schema = "LIBRARY")
public class Publishing {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @OneToOne
    @JoinColumn(name = "ADDRESS_ID")
    @JsonIgnoreProperties("publishing")
    private Address address;

    @OneToMany(mappedBy = "publishing")
    @JsonIgnoreProperties("publishing")
    private List<Book> books;
}
