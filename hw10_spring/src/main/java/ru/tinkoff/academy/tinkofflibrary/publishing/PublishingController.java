package ru.tinkoff.academy.tinkofflibrary.publishing;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/publishing")
@RequiredArgsConstructor
public class PublishingController {
    private final PublishingService publishingService;

    @PostMapping
    public Publishing savePublishing(@RequestBody Publishing publishing) {
        return publishingService.savePublishing(publishing);
    }

    @GetMapping
    public List<Publishing> findPublishings() {
        return publishingService.findPublishings();
    }

    @GetMapping("/{publishingId}")
    public ResponseEntity<Publishing> findPublishingById(@PathVariable Long publishingId) {
        Optional<Publishing> publishing = publishingService.findPublishingById(publishingId);

        if (publishing.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(publishing.get(), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<Publishing> updatePublishing(@RequestBody Publishing publishing) {
        Optional<Publishing> updatedPublishing = publishingService.updatePublishing(publishing);

        if (updatedPublishing.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(updatedPublishing.get(), HttpStatus.OK);
    }

    @DeleteMapping("/{publishingId}")
    public void deletePublishingById(@PathVariable Long publishingId) {
        publishingService.deletePublishingById(publishingId);
    }
}
