package ru.tinkoff.academy.tinkofflibrary.publishing;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PublishingRepository extends JpaRepository<Publishing, Long> {

}
