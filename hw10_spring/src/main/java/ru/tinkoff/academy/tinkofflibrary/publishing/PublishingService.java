package ru.tinkoff.academy.tinkofflibrary.publishing;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PublishingService {

    private final PublishingRepository publishingRepository;

    public Publishing savePublishing(Publishing publishing) {
        return publishingRepository.save(publishing);
    }

    public List<Publishing> findPublishings() {
        return publishingRepository.findAll();
    }

    public Optional<Publishing> findPublishingById(Long publishingId) {
        return publishingRepository.findById(publishingId);
    }

    public Optional<Publishing> updatePublishing(Publishing publishing) {
        Optional<Publishing> optionalPublishingFromDb = publishingRepository.findById(
            publishing.getId()
        );

        if (optionalPublishingFromDb.isEmpty()) {
            return Optional.empty();
        }

        Publishing publishingFromDb = optionalPublishingFromDb.get();

        publishingFromDb
            .setName(publishing.getName())
            .setAddress(publishing.getAddress())
            .setBooks(publishing.getBooks());

        return Optional.of(publishingRepository.save(publishingFromDb));
    }

    public void deletePublishingById(Long publishingId) {
        publishingRepository.deleteById(publishingId);
    }
}
