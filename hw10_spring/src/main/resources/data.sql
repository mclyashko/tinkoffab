INSERT INTO library.author (id, name) VALUES (1, 'Пелевин') ON CONFLICT DO NOTHING;
INSERT INTO library.author (id, name) VALUES (2, 'Быков') ON CONFLICT DO NOTHING;
INSERT INTO library.author (id, name) VALUES (3, 'Маяковский') ON CONFLICT DO NOTHING;

INSERT INTO library.genre (id, description, name) VALUES (1, 'Интересно', 'Жанр №1') ON CONFLICT DO NOTHING;
INSERT INTO library.genre (id, description, name) VALUES (2, 'Познавательно', 'Жанр №2') ON CONFLICT DO NOTHING;
INSERT INTO library.genre (id, description, name) VALUES (3, 'Любознательно', 'Жанр №3') ON CONFLICT DO NOTHING;

INSERT INTO library.publishing (id, name) VALUES (2, 'ПИФ') ON CONFLICT DO NOTHING;
INSERT INTO library.publishing (id, name) VALUES (3, 'Азбука') ON CONFLICT DO NOTHING;
INSERT INTO library.publishing (id, name) VALUES (1, 'МИФ') ON CONFLICT DO NOTHING;

INSERT INTO library.address (id, city, country, house, street) VALUES (1, 'Москва', 'Россия', '13', 'ул. Ленина') ON CONFLICT DO NOTHING;
INSERT INTO library.address (id, city, country, house, street) VALUES (2, 'Питер', 'Россия', '9', 'ул. Блюхера') ON CONFLICT DO NOTHING;
INSERT INTO library.address (id, city, country, house, street) VALUES (3, 'Челябинск', 'Россия', '1', 'ул. Карла Маркса') ON CONFLICT DO NOTHING;

UPDATE library.publishing SET address_id = 1 WHERE id = 1;
UPDATE library.publishing SET address_id = 2 WHERE id = 2;
UPDATE library.publishing SET address_id = 3 WHERE id = 3;

UPDATE library.address SET publishing_id = 1 WHERE id = 1;
UPDATE library.address SET publishing_id = 2 WHERE id = 2;
UPDATE library.address SET publishing_id = 3 WHERE id = 3;

INSERT INTO library.book (id, publication_date, title, genre_id, publishing_id) VALUES (1, '2022-11-03', 'Интересная книга Пелевина', 1, 1) ON CONFLICT DO NOTHING;
INSERT INTO library.book (id, publication_date, title, genre_id, publishing_id) VALUES (2, '2022-11-03', 'Познавательная книга Быкова', 2, 2) ON CONFLICT DO NOTHING;
INSERT INTO library.book (id, publication_date, title, genre_id, publishing_id) VALUES (3, '2022-11-18', 'Любознательная книга Маяковского', 3, 3) ON CONFLICT DO NOTHING;

INSERT INTO library.book_mapping_author (book_mapping_id, author_mapping_id) VALUES (1, 1) ON CONFLICT DO NOTHING;
INSERT INTO library.book_mapping_author (book_mapping_id, author_mapping_id) VALUES (2, 2) ON CONFLICT DO NOTHING;
INSERT INTO library.book_mapping_author (book_mapping_id, author_mapping_id) VALUES (3, 3) ON CONFLICT DO NOTHING;