create schema if not exists library;

create table if not exists library.author
(
    id   bigint generated by default as identity,
    name varchar(255),
    primary key (id)
);

create table if not exists library.genre
(
    id          bigint generated by default as identity,
    description varchar(255),
    name        varchar(255),
    primary key (id)
);

create table if not exists library.publishing
(
    id         bigint generated by default as identity,
    name       varchar(255),
    address_id bigint,
    primary key (id)
);

create table if not exists library.address
(
    id            bigint generated by default as identity,
    city          varchar(255),
    country       varchar(255),
    house         varchar(255),
    street        varchar(255),
    publishing_id bigint,
    primary key (id)
);

alter table library.publishing drop constraint if exists fkjg3xs9483pats6nsh8t5sbjop;

alter table library.publishing add
    constraint fkjg3xs9483pats6nsh8t5sbjop
        foreign key (address_id) references library.address;

alter table library.address drop constraint if exists fkk69wul3hii7mxevourivfl8qa;

alter table library.address add
    constraint fkk69wul3hii7mxevourivfl8qa
        foreign key (publishing_id) references library.publishing;

create table if not exists library.book
(
    id               bigint generated by default as identity,
    publication_date date,
    title            varchar(255),
    genre_id         bigint,
    publishing_id    bigint,
    primary key (id),
    constraint fkm1t3yvw5i7olwdf32cwuul7ta
        foreign key (genre_id) references library.genre,
    constraint fkdj2ayuksak2l2wgl2q9emj3h4
        foreign key (publishing_id) references library.publishing
);

create table if not exists library.book_mapping_author
(
    book_mapping_id   bigint not null,
    author_mapping_id bigint not null,
    primary key (author_mapping_id, book_mapping_id),
    constraint fk675jl527q7uxrbmvdpqgm1eq7
        foreign key (author_mapping_id) references library.author,
    constraint fkbas9nlrpk3gcf4ejhkwiuj0th
        foreign key (book_mapping_id) references library.book
);