package ru.mirea.hw2.tictactoe;

import static ru.mirea.hw2.tictactoe.game.TicTacToe.startGame;

public class Main {
    public static void main(String[] args) {
        startGame();
    }
}
