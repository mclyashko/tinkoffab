package ru.mirea.hw2.tictactoe.game;

import javafx.util.Pair;
import ru.mirea.hw2.tictactoe.map.GameMap;
import ru.mirea.hw2.tictactoe.map.point.Point;
import ru.mirea.hw2.tictactoe.map.point.PointValue;
import ru.mirea.hw2.tictactoe.player.implementation.Player;
import ru.mirea.hw2.tictactoe.player.PlayerType;

import java.util.Scanner;

import static ru.mirea.hw2.tictactoe.player.PlayerFabric.makePlayer;

public class InteractiveUserInterface {
    private final static String AFFIRMATIVE_USER_ANSWER = "yes";
    private final static String NEGATIVE_USER_ANSWER = "no";
    private final static int MIN_MAP_SIZE = 3;
    private final static int MAX_MAP_SIZE = 5;
    private final static String INVITATION_TO_CHOOSE_FIRST_PLAYER = "Hi! Would you like to be the first player? [yes/no]: ";
    private final static String INVITATION_TO_CHOOSE_MAP_SIZE = "Enter the map size, please: ";
    private final static String INVITATION_TO_CHOOSE_POINT = "Next step: ";
    private final static String WRONG_INPUT_WARNING = "Input is wrong. Try again;";

    public static Pair<Player, Player> getPlayersFromCLI() {
        Scanner scanner = new Scanner(System.in);

        Pair<Player, Player> players;

        while (true) {
            System.out.println(INVITATION_TO_CHOOSE_FIRST_PLAYER);

            String inputStrategy = scanner.nextLine();

            switch (inputStrategy) {
                case AFFIRMATIVE_USER_ANSWER -> {
                    players = new Pair<>(makePlayer(PlayerType.HUMAN, PointValue.X),
                            makePlayer(PlayerType.AI, PointValue.O));
                    return players;
                }
                case NEGATIVE_USER_ANSWER -> {
                    players = new Pair<>(makePlayer(PlayerType.AI, PointValue.X),
                            makePlayer(PlayerType.HUMAN, PointValue.O));
                    return players;
                }
            }
        }
    }

    public static Point getMoveFromCLI() {
        Scanner scanner = new Scanner(System.in);

        System.out.println(INVITATION_TO_CHOOSE_POINT);

        String[] dataArray = scanner.nextLine().split(" ");
        int x = Integer.parseInt(dataArray[0]) - 1;
        int y = Integer.parseInt(dataArray[1]) - 1;

        return new Point(x, y);
    }

    public static int getMapSizeFromCLI() {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println(INVITATION_TO_CHOOSE_MAP_SIZE);

            String[] dataArray = scanner.nextLine().split(" ");
            int size = Integer.parseInt(dataArray[0]);

            if (MIN_MAP_SIZE <= size && size <= MAX_MAP_SIZE) {
                return size;
            }
        }
    }

    public static void printWarningToCLI() {
        System.out.println(WRONG_INPUT_WARNING);
    }

    public static void printMapToCLI(GameMap gameMap) {
        for (int i = 0; i < gameMap.getSize(); i++) {
            for (int j = 0; j < gameMap.getSize(); j++) {
                Point point = new Point(i, j);
                System.out.printf("%s\t", gameMap.get(point));
            }
            System.out.println();
        }
    }
}
