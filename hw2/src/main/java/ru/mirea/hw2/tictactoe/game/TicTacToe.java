package ru.mirea.hw2.tictactoe.game;

import javafx.util.Pair;
import ru.mirea.hw2.tictactoe.map.TicTacToeMap;
import ru.mirea.hw2.tictactoe.map.GameMap;
import ru.mirea.hw2.tictactoe.map.point.Point;
import ru.mirea.hw2.tictactoe.player.implementation.Player;

public class TicTacToe {
    public static void startGame() {
        Pair<Player, Player> players = InteractiveUserInterface.getPlayersFromCLI();

        int mapSize = InteractiveUserInterface.getMapSizeFromCLI();
        GameMap gameMap = new TicTacToeMap(mapSize);

        runGame(players, gameMap);
    }

    private static void saveMoveFromPlayer(Player player, GameMap gameMap) {
        Point move = player.makeMove(gameMap);

        gameMap.set(move, player.getPlayerSymbol());
    }

    private static void runGame(Pair<Player, Player> players, GameMap gameMap) {

        while (true) {
            saveMoveFromPlayer(players.getKey(), gameMap);
            if (gameMap.isEnd()) {
                break;
            }

            InteractiveUserInterface.printMapToCLI(gameMap);

            saveMoveFromPlayer(players.getValue(), gameMap);
            if (gameMap.isEnd()) {
                break;
            }

            InteractiveUserInterface.printMapToCLI(gameMap);
        }

        InteractiveUserInterface.printMapToCLI(gameMap);
    }
}
