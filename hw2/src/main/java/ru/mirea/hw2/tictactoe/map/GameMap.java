package ru.mirea.hw2.tictactoe.map;

import ru.mirea.hw2.tictactoe.map.point.Point;
import ru.mirea.hw2.tictactoe.map.point.PointValue;
public interface GameMap {

    int getSize();

    boolean isPointInvalid(Point point);

    void set(Point point, PointValue pointValue);

    PointValue get(Point point);

    boolean isWin(PointValue pointValue);

    boolean isEnd();

    GameMap getCopy();
}
