package ru.mirea.hw2.tictactoe.map;

import ru.mirea.hw2.tictactoe.map.point.Point;
import ru.mirea.hw2.tictactoe.map.point.PointValue;

public class TicTacToeMap implements GameMap {

    private final int mapSize;
    private final PointValue[][] map;

    public TicTacToeMap(int size) {
        map = new PointValue[size][size];

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                map[i][j] = PointValue.EMPTY;
            }
        }

        this.mapSize = size;
    }

    @Override
    public int getSize() {
        return this.mapSize;
    }

    @Override
    public boolean isPointInvalid(Point point) {
        return point.x() < 0 || point.x() >= mapSize ||
            point.y() < 0 || point.y() >= mapSize
            || this.get(point) != PointValue.EMPTY;
    }

    @Override
    public void set(Point point, PointValue pointValue) {
        map[point.x()][point.y()] = pointValue;
    }

    @Override
    public PointValue get(Point point) {
        return map[point.x()][point.y()];
    }

    @Override
    public boolean isWin(PointValue pointValue) {
        return checkLines(pointValue) || checkColumns(pointValue) || checkDiagonals(pointValue);
    }

    @Override
    public boolean isEnd() {
        return checkMapIsFull() || isWin(PointValue.X) || isWin(PointValue.O);
    }

    @Override
    public GameMap getCopy() {
        GameMap copy = new TicTacToeMap(mapSize);

        for (int i = 0; i < mapSize; i++) {
            for (int j = 0; j < mapSize; j++) {
                Point point = new Point(i, j);
                copy.set(point, map[i][j]);
            }
        }

        return copy;
    }

    private boolean checkMapIsFull() {
        for (int i = 0; i < mapSize; i++) {
            for (int j = 0; j < mapSize; j++) {
                if (map[i][j] == PointValue.EMPTY) {
                    return false;
                }
            }
        }

        return true;
    }

    private boolean checkLines(PointValue pointValue) {
        for (int i = 0; i < mapSize; i++) {
            boolean isLineFilled = true;
            for (int j = 0; j < mapSize; j++) {
                if (map[i][j] != pointValue) {
                    isLineFilled = false;
                    break;
                }
            }

            if (isLineFilled) {
                return true;
            }
        }

        return false;
    }

    private boolean checkColumns(PointValue pointValue) {
        for (int i = 0; i < mapSize; i++) {
            boolean isColumnFilled = true;
            for (int j = 0; j < mapSize; j++) {
                if (map[j][i] != pointValue) {
                    isColumnFilled = false;
                    break;
                }
            }

            if (isColumnFilled) {
                return true;
            }
        }

        return false;
    }

    private boolean checkDiagonals(PointValue pointValue) {
        boolean firstDiagonal = true;
        boolean secondDiagonal = true;

        for (int i = 0; i < mapSize; i++) {
            if (map[i][i] != pointValue) {
                firstDiagonal = false;
            }

            if (map[i][mapSize - i - 1] != pointValue) {
                secondDiagonal = false;
            }
        }

        return firstDiagonal || secondDiagonal;
    }

}
