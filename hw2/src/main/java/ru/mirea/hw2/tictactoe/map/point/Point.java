package ru.mirea.hw2.tictactoe.map.point;

public record Point(int x, int y) {

}
