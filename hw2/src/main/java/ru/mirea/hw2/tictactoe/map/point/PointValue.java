package ru.mirea.hw2.tictactoe.map.point;

public enum PointValue {
    X("X"), O("O"), EMPTY("-");

    private final String value;

    PointValue(String value) {
        this.value = value;
    }


    @Override
    public String toString() {
        return this.value;
    }
}
