package ru.mirea.hw2.tictactoe.player;

import ru.mirea.hw2.tictactoe.map.point.PointValue;
import ru.mirea.hw2.tictactoe.player.implementation.ArtificialIntelligencePlayer;
import ru.mirea.hw2.tictactoe.player.implementation.HumanPlayer;
import ru.mirea.hw2.tictactoe.player.implementation.Player;

public class PlayerFabric {
    public static Player makePlayer(PlayerType type, PointValue value) {
        return switch (type) {
            case AI -> new ArtificialIntelligencePlayer(value);
            case HUMAN -> new HumanPlayer(value);
        };
    }
}
