package ru.mirea.hw2.tictactoe.player;

public enum PlayerType {
    HUMAN,
    AI
}
