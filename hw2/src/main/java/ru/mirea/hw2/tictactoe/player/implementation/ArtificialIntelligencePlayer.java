package ru.mirea.hw2.tictactoe.player.implementation;

import ru.mirea.hw2.tictactoe.map.GameMap;
import ru.mirea.hw2.tictactoe.map.point.Point;
import ru.mirea.hw2.tictactoe.map.point.PointValue;
import ru.mirea.hw2.tictactoe.util.Minimax;
import ru.mirea.hw2.tictactoe.util.MinimaxEnum;

public class ArtificialIntelligencePlayer implements Player {

    private final PointValue symbol;

    public ArtificialIntelligencePlayer(PointValue symbol) {
        this.symbol = symbol;
    }

    @Override
    public PointValue getPlayerSymbol() {
        return this.symbol;
    }

    @Override
    public Point makeMove(GameMap gameMap) {

        Point point = null;
        int bestScore = Integer.MIN_VALUE;
        GameMap gameMapCopy = gameMap.getCopy();

        Minimax minimax = new Minimax(gameMapCopy, getPlayerSymbol(), calculateEnemySymbol());

        for (int i = 0; i < gameMapCopy.getSize(); i++) {
            for (int j = 0; j < gameMapCopy.getSize(); j++) {
                Point nextPoint = new Point(i, j);

                // Поиск оптимального хода в пустую клетку на основе рейтинга minimax
                if (gameMapCopy.get(nextPoint) == PointValue.EMPTY) {
                    gameMapCopy.set(nextPoint, getPlayerSymbol());

                    int score = minimax.runMinimaxAlgo(MinimaxEnum.HUMAN);

                    gameMapCopy.set(nextPoint, PointValue.EMPTY);

                    // Обновление лучшего результата
                    if (score > bestScore) {
                        bestScore = score;
                        point = nextPoint;
                    }
                }
            }
        }

        return point;
    }

    private PointValue calculateEnemySymbol() {
        return switch (getPlayerSymbol()) {
            case X -> PointValue.O;
            case O -> PointValue.X;
            case EMPTY -> PointValue.EMPTY;
        };
    }
}
