package ru.mirea.hw2.tictactoe.player.implementation;

import ru.mirea.hw2.tictactoe.map.GameMap;
import ru.mirea.hw2.tictactoe.map.point.Point;
import ru.mirea.hw2.tictactoe.map.point.PointValue;

import static ru.mirea.hw2.tictactoe.game.InteractiveUserInterface.getMoveFromCLI;
import static ru.mirea.hw2.tictactoe.game.InteractiveUserInterface.printWarningToCLI;

public class HumanPlayer implements Player {

    private final PointValue symbol;

    public HumanPlayer(PointValue symbol) {
        this.symbol = symbol;
    }

    @Override
    public PointValue getPlayerSymbol() {
        return this.symbol;
    }

    @Override
    public Point makeMove(GameMap gameMap) {
        while (true) {
            Point point = getMoveFromCLI();

            if (gameMap.isPointInvalid(point)) {
                printWarningToCLI();
            } else {
                return point;
            }
        }
    }
}
