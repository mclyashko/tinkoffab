package ru.mirea.hw2.tictactoe.player.implementation;

import ru.mirea.hw2.tictactoe.map.GameMap;
import ru.mirea.hw2.tictactoe.map.point.Point;
import ru.mirea.hw2.tictactoe.map.point.PointValue;

public interface Player {

    PointValue getPlayerSymbol();

    Point makeMove(GameMap gameMap);
}
