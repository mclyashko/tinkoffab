package ru.mirea.hw2.tictactoe.util;

import ru.mirea.hw2.tictactoe.map.GameMap;
import ru.mirea.hw2.tictactoe.map.point.Point;
import ru.mirea.hw2.tictactoe.map.point.PointValue;

import java.util.function.BiFunction;

public class Minimax {

    private final GameMap gameMap;
    private final PointValue botSymbol;
    private final PointValue humanSymbol;

    public Minimax(GameMap gameMap, PointValue botSymbol, PointValue humanSymbol) {
        this.gameMap = gameMap;
        this.botSymbol = botSymbol;
        this.humanSymbol = humanSymbol;
    }

    public int runMinimaxAlgo(MinimaxEnum type) {
        // Возвращаем очки победы в случае победы AI, Человека или ничьей соответственно
        if (gameMap.isWin(botSymbol)) {
            return MinimaxEnum.AI.getWeight();
        }
        if (gameMap.isWin(humanSymbol)) {
            return MinimaxEnum.HUMAN.getWeight();
        }
        if (gameMap.isEnd()) {
            return MinimaxEnum.DRAW.getWeight();
        }

        int bestScore = 0;

        if (type == MinimaxEnum.AI) {
            bestScore = calculateScore(type, Integer.MIN_VALUE, (a, b) -> (a >= b) ? a : b,
                botSymbol);
        } else if (type == MinimaxEnum.HUMAN) {
            bestScore = calculateScore(type, Integer.MAX_VALUE, (a, b) -> (a <= b) ? a : b,
                humanSymbol);
        }

        return bestScore;
    }

    private int calculateScore(MinimaxEnum type, int bestScoreInit,
        BiFunction<Integer, Integer, Integer> optimizeFunction, PointValue symbolToPrint) {
        for (int i = 0; i < gameMap.getSize(); i++) {
            for (int j = 0; j < gameMap.getSize(); j++) {
                Point nextPoint = new Point(i, j);

                // Ищем пустые клетки, ставим в них требуемый символ
                if (gameMap.get(nextPoint) == PointValue.EMPTY) {
                    gameMap.set(nextPoint, symbolToPrint);
                    int score = runMinimaxAlgo(nextType(type));
                    gameMap.set(nextPoint, PointValue.EMPTY);

                    // Производим подсчет
                    bestScoreInit = optimizeFunction.apply(bestScoreInit, score);
                }
            }
        }

        return bestScoreInit;
    }

    private MinimaxEnum nextType(MinimaxEnum type) {
        return switch (type) {
            case HUMAN -> MinimaxEnum.AI;
            case AI -> MinimaxEnum.HUMAN;
            default -> throw new RuntimeException("No such minimax type!");
        };
    }
}
