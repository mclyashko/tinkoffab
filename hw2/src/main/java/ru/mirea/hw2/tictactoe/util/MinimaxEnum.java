package ru.mirea.hw2.tictactoe.util;

public enum MinimaxEnum {
    AI(10), HUMAN(-10), DRAW(0);
    private final int weight;

    MinimaxEnum(int weight) {
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }
}
