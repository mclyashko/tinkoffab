package ru.mirea.hw2.tictactoe.map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.mirea.hw2.tictactoe.map.point.Point;
import ru.mirea.hw2.tictactoe.map.point.PointValue;

class TicTacToeMapTest {

    public static final int MAP_SIZE = 3;
    GameMap gameMap;

    @BeforeEach
    void setUp() {
        gameMap = new TicTacToeMap(MAP_SIZE);
    }

    @Test
    void getSize() {
        int actualSize = gameMap.getSize();

        Assertions.assertEquals(MAP_SIZE, actualSize);
    }

    @Test
    void isMapPointInvalid() {
        boolean isPointInvalid = gameMap.isPointInvalid(new Point(MAP_SIZE, MAP_SIZE + 1));

        Assertions.assertTrue(isPointInvalid);
    }

    @Test
    void setAndGetSomeValidPointOnMap() {
        Point point = new Point(MAP_SIZE - 1, MAP_SIZE - 1);
        PointValue value = PointValue.X;

        gameMap.set(point, value);

        Assertions.assertEquals(value, gameMap.get(point));
    }

    @Test
    void isItGameWin() {
        PointValue pointValue = PointValue.O;
        for(int i = 0; i < MAP_SIZE; i++){
            Point point = new Point(i, i);
            gameMap.set(point, pointValue);
        }

        Assertions.assertTrue(gameMap.isWin(pointValue));
    }
}