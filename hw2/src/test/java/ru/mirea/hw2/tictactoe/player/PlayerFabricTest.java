package ru.mirea.hw2.tictactoe.player;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.mirea.hw2.tictactoe.map.point.PointValue;
import ru.mirea.hw2.tictactoe.player.implementation.ArtificialIntelligencePlayer;
import ru.mirea.hw2.tictactoe.player.implementation.HumanPlayer;
import ru.mirea.hw2.tictactoe.player.implementation.Player;

class PlayerFabricTest {

    @Test
    void makeHumanPlayer() {
        Player humanPlayer = PlayerFabric.makePlayer(PlayerType.HUMAN, PointValue.O);

        Assertions.assertInstanceOf(HumanPlayer.class, humanPlayer);
    }

    @Test
    void makeArtificialIntelligencePlayer() {
        Player artificialIntelligencePlayer = PlayerFabric.makePlayer(PlayerType.AI, PointValue.X);

        Assertions.assertInstanceOf(ArtificialIntelligencePlayer.class, artificialIntelligencePlayer);
    }

    @Test
    void makeXPlayer() {
        Player humanPlayer = PlayerFabric.makePlayer(PlayerType.HUMAN, PointValue.X);

        Assertions.assertEquals(PointValue.X, humanPlayer.getPlayerSymbol());
    }

    @Test
    void makeYPlayer() {
        Player humanPlayer = PlayerFabric.makePlayer(PlayerType.HUMAN, PointValue.O);

        Assertions.assertEquals(PointValue.O, humanPlayer.getPlayerSymbol());
    }
}