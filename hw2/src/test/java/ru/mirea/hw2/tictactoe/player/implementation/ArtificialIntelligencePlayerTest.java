package ru.mirea.hw2.tictactoe.player.implementation;

import java.util.stream.Stream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import ru.mirea.hw2.tictactoe.map.GameMap;
import ru.mirea.hw2.tictactoe.map.TicTacToeMap;
import ru.mirea.hw2.tictactoe.map.point.Point;
import ru.mirea.hw2.tictactoe.map.point.PointValue;
import ru.mirea.hw2.tictactoe.player.PlayerFabric;
import ru.mirea.hw2.tictactoe.player.PlayerType;

class ArtificialIntelligencePlayerTest {

    public static final int MAP_SIZE = 3;
    GameMap gameMap;
    Player botPlayer;

    @BeforeEach
    void setUp() {
        gameMap = new TicTacToeMap(MAP_SIZE);
        botPlayer = PlayerFabric.makePlayer(PlayerType.AI, PointValue.X);
    }

    @ParameterizedTest
    @MethodSource("makeLastMoveArguments")
    void makeLastMove(TicTacToeMapInfo[] mapInfos, Point nextStep) {
        fillMap(mapInfos);

        Point actualStep = botPlayer.makeMove(gameMap);

        Assertions.assertEquals(nextStep, actualStep);
    }

    private static Stream<Arguments> makeLastMoveArguments() {
        return Stream.of(Arguments.of(
                new TicTacToeMapInfo[]{new TicTacToeMapInfo(new Point(0, 0), PointValue.X),
                    new TicTacToeMapInfo(new Point(0, 1), PointValue.O),
                    new TicTacToeMapInfo(new Point(0, 2), PointValue.X),
                    new TicTacToeMapInfo(new Point(1, 0), PointValue.O),
                    new TicTacToeMapInfo(new Point(1, 1), PointValue.X),
                    new TicTacToeMapInfo(new Point(1, 2), PointValue.O)
                },
                new Point(2, 0)
            ),
            Arguments.of(
                new TicTacToeMapInfo[] {
                    new TicTacToeMapInfo(new Point(0, 0), PointValue.X),
                    new TicTacToeMapInfo(new Point(0, 1), PointValue.O),
                    new TicTacToeMapInfo(new Point(0, 2), PointValue.X),
                    new TicTacToeMapInfo(new Point(1, 0), PointValue.O),
                    new TicTacToeMapInfo(new Point(2, 0), PointValue.X),
                    new TicTacToeMapInfo(new Point(2, 2), PointValue.O),
                },
                new Point(1, 1)
            ),
            Arguments.of(
                new TicTacToeMapInfo[] {
                    new TicTacToeMapInfo(new Point(0, 1), PointValue.X),
                    new TicTacToeMapInfo(new Point(1, 1), PointValue.X),
                    new TicTacToeMapInfo(new Point(1, 2), PointValue.O),
                    new TicTacToeMapInfo(new Point(2, 0), PointValue.X),
                    new TicTacToeMapInfo(new Point(2, 1), PointValue.O),
                    new TicTacToeMapInfo(new Point(2, 2), PointValue.O),
                },
                new Point(0, 2)
            )
        );
    }

    private void fillMap(TicTacToeMapInfo[] mapInfos) {
        for (TicTacToeMapInfo mapInfo : mapInfos) {
            gameMap.set(mapInfo.point, mapInfo.value);
        }
    }

    private record TicTacToeMapInfo(Point point, PointValue value) {

    }
}