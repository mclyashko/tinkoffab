package ru.mirea.seminar;

public enum Permission {
    ADD, EDIT, SEARCH, DELETE
}
