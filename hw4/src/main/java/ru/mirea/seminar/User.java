package ru.mirea.seminar;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class User {

    private Long id;
    private String name;
    private int age;
    private Set<Role> roles = new HashSet<>();

    @SuppressWarnings("unused")
    private Car car;

    @SuppressWarnings("unused")
    public User(long id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    @SuppressWarnings("unused")
    public User() {
        this.id = 0L;
        this.name = "name";
        this.age = 0;
    }

    @SuppressWarnings("unused")
    public Long getId() {
        return id;
    }

    @SuppressWarnings("unused")
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @SuppressWarnings("unused")
    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    @SuppressWarnings("unused")
    public void setAge(int age) {
        this.age = age;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    @SuppressWarnings("unused")
    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }


    @SuppressWarnings("unused")
    public static class Car {

        @SuppressWarnings("unused")
        String name;

        @SuppressWarnings("unused")
        public String getName() {
            return name;
        }
    }

    @SuppressWarnings("unused")
    public Optional<Car> getCar() {
        return Optional.of(this.car);
    }
}
