package ru.mirea.seminar.homework.lambdas;

import java.util.HashSet;
import ru.mirea.seminar.Permission;
import ru.mirea.seminar.Role;
import ru.mirea.seminar.User;

import java.util.Set;

@SuppressWarnings("unused")
public class Task1 {

    private final Set<User> users = new HashSet<>();

    /**
     * Было -  использование Iterator<User>, if + anyMatch + then remove. Стало - использование
     * removeIf. Условие удаления переработано и вынесено в метод hasUserPermission. Метод
     * hasUserPermission использует вспомогательный метод hasRolePermission.
     */

    public void removeUsersWithPermission(Permission permission) {
        users.removeIf(user -> hasUserPermission(user, permission));
    }

    private boolean hasUserPermission(User user, Permission permission) {
        return user.getRoles().stream()
            .anyMatch(role -> hasRolePermission(role, permission));
    }

    private boolean hasRolePermission(Role role, Permission permission) {
        return role.getPermissions().contains(permission);
    }
}