package ru.mirea.seminar.homework.lambdas;

import ru.mirea.seminar.Role;
import ru.mirea.seminar.User;

import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;

@SuppressWarnings("unused")
public class Task2 {

    private final Map<String, Set<User>> usersByRole = new HashMap<>();

    /**
     * Было - сложная лямбда внутри forEach, незадействованный метод getUsersByRole. Стало - простая
     * лямбда внутри forEach, доп. метод, обертывающий getUsersByRole
     */

    @SuppressWarnings("unused")
    public void addUser(User user) {
        user.getRoles().forEach(role -> addUserToUsersByRole(role, user));
    }

    private void addUserToUsersByRole(Role role, User user) {
        Set<User> usersInRole = getUsersByRole(role.getName());
        usersInRole.add(user);
        usersByRole.put(role.getName(), usersInRole);
    }

    private Set<User> getUsersByRole(String role) {
        return usersByRole.getOrDefault(role, Collections.emptySet());
    }
}
