package ru.mirea.seminar.homework.optional;


import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SuppressWarnings("unused")
public class Task1 {

    /**
     * Было - использование Optional в качестве параметра. Метод getAttachment не оборачивал поле
     * attachment в ofNullable Optional. "Раздутый" класс Attachment. Стало - отсутствие Optional в
     * параметрах. Корректная обработка внутри getAttachment. Класс Attachment теперь record.
     */

    public List<Email> create() {
        Email noAttachment = new Email("First!", "No attachment", null);
        Attachment attachment = new Attachment("/mnt/files/image.png", 370);
        Email withAttachment = new Email("Second!", "With attachment", attachment);
        return Arrays.asList(noAttachment, withAttachment);
    }

    static class Email implements Serializable {

        private final String subject;
        private final String body;
        private final Attachment attachment;

        Email(String subject, String body, Attachment attachment) {
            this.subject = subject;
            this.body = body;
            this.attachment = attachment;
        }

        String getSubject() {
            return subject;
        }

        String getBody() {
            return body;
        }

        Optional<Attachment> getAttachment() {
            return Optional.ofNullable(attachment);
        }
    }


    @SuppressWarnings("SameParameterValue")
    record Attachment(String path, int size) {

    }
}
