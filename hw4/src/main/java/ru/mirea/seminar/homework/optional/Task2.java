package ru.mirea.seminar.homework.optional;

import ru.mirea.seminar.User;

import java.util.List;
import java.util.Optional;

@SuppressWarnings("unused")
public class Task2 {

    private static final String ADMIN_ROLE = "admin";

    /**
     * Было - двойная проверка (isPresent и isEmpty. Зачем? Если isEmpty, то !isPresent).
     * Использование if вместо orElseThrow. Стало - orElseThrow.
     */

    public Optional<User> findAnyAdmin() {
        Optional<List<User>> users = findUsersByRole(ADMIN_ROLE);

        return users.map(usr -> usr.get(0)); // Optional ofNullable admin #0 or empty optional
    }

    @SuppressWarnings("SameParameterValue")
    private Optional<List<User>> findUsersByRole(String role) {
        //real search in DB
        return Optional.empty();
    }
}
