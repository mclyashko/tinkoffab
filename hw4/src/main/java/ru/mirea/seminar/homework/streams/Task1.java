package ru.mirea.seminar.homework.streams;

import ru.mirea.seminar.User;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toMap;

@SuppressWarnings("unused")
public class Task1 {

    /**
     * Было - большой двойной Stream. Стало - один toMap.
     */

    public Map<String, Integer> getMaxAgeByUserName(List<User> users) {
        return users.stream().collect(toMap(User::getName, User::getAge, Math::max));
    }
}
