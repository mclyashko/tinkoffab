package ru.mirea.seminar.homework.streams;

import ru.mirea.seminar.User;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("unused")
public class Task2 {

    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    private final Set<User> users = new HashSet<>();

    /**
     * Было - использование неспециализированных Stream. Стало - использование IntStream
     */

    public int getTotalAge() {
        return users.stream().mapToInt(User::getAge).sum();
    }

    public int countEmployees(Map<String, List<User>> departments) {
        return departments.values().stream().mapToInt(List::size).sum();
    }
}
