package ru.mirea.seminar.homework.streams;


import java.util.Arrays;
import ru.mirea.seminar.User;

import java.util.List;

@SuppressWarnings("unused")
public class Task3 {

    /**
     * Было - неэффективный код (из-за невозможности повторного использования Stream). Стало -
     * повторное использование nameLengthArray
     */
    public void printNameStats(List<User> users) {

        int[] nameLengthArray = getNameLengthArray(users);

        Arrays.stream(nameLengthArray).max().ifPresent(max -> System.out.println("MAX: " + max));
        Arrays.stream(nameLengthArray).min().ifPresent(min -> System.out.println("MIN: " + min));
    }

    private int[] getNameLengthArray(List<User> users) {
        return users.stream().mapToInt(user -> user.getName().length()).toArray();
    }
}
