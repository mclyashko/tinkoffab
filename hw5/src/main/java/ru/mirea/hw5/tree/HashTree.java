package ru.mirea.hw5.tree;

import java.util.HashSet;
import java.util.Iterator;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;

/**
 Формализация задачи:

 Требуется следующая структура данных:

 Взятие размера O(1)
 Проверка на вхождение O(1)
 Вставка O(log n)
 Удаление O(log n)
 Поиск минимального O(log n)
 Поиск максимального O(log n)
 Получение итератора на отсортированную последовательность
 Возможность извлечь К-Ч бинарное дерево
 Возможность очистки

 Память 2 x N
 */

public class HashTree<T extends Comparable<T>> {

    private final Set<T> set = new HashSet<>();
    private final TreeMap<T, Object> redBlackTree = new TreeMap<>();

    public int size() {
        return set.size();
    }

    public boolean contains(T value) {
        return set.contains(value);
    }

    public void put(T value) {
        if (contains(value)) {
            return;
        }

        set.add(value);
        redBlackTree.put(value, null);
    }

    public void remove(T value) {
        if (!contains(value)) {
            return;
        }

        set.remove(value);
        redBlackTree.remove(value);
    }

    public T firstKey() {
        return redBlackTree.firstKey();
    }

    public T lastKey() {
        return redBlackTree.lastKey();
    }

    public Iterator<T> ascendingOrderIterator() {
        return redBlackTree.keySet().iterator();
    }

    public TreeMap<T, Object> getRedBlackTree() {
        return redBlackTree;
    }

    public void clear(){
        set.clear();
        redBlackTree.clear();
    }

}
