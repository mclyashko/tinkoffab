package ru.mirea.hw5.tree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class HashTreeTest {

    HashTree<String> hashTree;

    @BeforeEach
    void setUp() {
        hashTree = new HashTree<>();
    }

    @Test
    void size() {
        hashTree.put("A");
        hashTree.put("C");
        hashTree.put("B");

        Assertions.assertEquals(3, hashTree.size());
    }

    @Test
    void contains() {
        hashTree.put("A");
        hashTree.put("C");
        hashTree.put("B");

        Assertions.assertFalse(hashTree.contains("E"));
    }

    @Test
    void put() {
        hashTree.put("A");

        Assertions.assertTrue(hashTree.contains("A"));
    }

    @Test
    void remove() {
        hashTree.put("A");
        hashTree.put("C");
        hashTree.put("B");

        hashTree.remove("C");

        Assertions.assertFalse(hashTree.contains("C"));
    }

    @Test
    void firstKey() {
        hashTree.put("A");
        hashTree.put("C");
        hashTree.put("B");

        Assertions.assertEquals("A", hashTree.firstKey());
    }

    @Test
    void lastKey() {
        hashTree.put("A");
        hashTree.put("C");
        hashTree.put("B");

        Assertions.assertEquals("C", hashTree.lastKey());
    }

    @Test
    void ascendingOrderIterator() {
        List<String> toPut = Arrays.asList("A", "C", "B");
        for (String s : toPut) {
            hashTree.put(s);
        }

        toPut.sort(Comparator.naturalOrder());
        List<String> actual = new ArrayList<>();
        var iter = hashTree.ascendingOrderIterator();
        while (iter.hasNext()) {
            actual.add(iter.next());
        }

        Assertions.assertEquals(toPut, actual);
    }

    @Test
    void clear() {
        hashTree.put("A");
        hashTree.put("C");
        hashTree.put("B");

        hashTree.clear();

        Assertions.assertEquals(0, hashTree.size());
    }
}