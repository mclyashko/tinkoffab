package ru.mirea.hw7.passgener;

import java.util.Map;
import ru.mirea.hw7.passgener.app.PassGenerApp;

public class Main {
    public static void main(String[] args) {
        Map<String, String> environmentVariables = System.getenv();

        PassGenerApp application = new PassGenerApp(environmentVariables);

        System.out.println(application.generate());
    }
}