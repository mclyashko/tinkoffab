package ru.mirea.hw7.passgener.app;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.random.RandomGenerator;
import ru.mirea.hw7.passgener.generator.ChunkProvider;
import ru.mirea.hw7.passgener.generator.PasswordGenerator;
import ru.mirea.hw7.passgener.util.securerandom.SecureRandomGetter;

public class PassGenerApp {

    private final PasswordGenerator passwordGenerator;

    public PassGenerApp(Map<String, String> environmentVariables) {
        passwordGenerator = makePasswordGenerator(
            environmentVariables,
            makeChunkProvider(
                environmentVariables,
                makeRandomGenerator()
            )
        );
    }

    public String generate() {
        return passwordGenerator.generate();
    }

    private RandomGenerator makeRandomGenerator() {
        return SecureRandomGetter.getInstance(
            PassGenerConstants.SECURE_RANDOM_ALGORITHM);
    }

    private ChunkProvider makeChunkProvider(Map<String, String> environmentVariables,
        RandomGenerator randomGenerator
    ) {
        List<String> filesWithChunksSources = Arrays.asList(
            environmentVariables.get(
                PassGenerConstants.FILES_WITH_CHUNKS_SOURCES_ENV_VAR_NAME
            ).split(PassGenerConstants.FILES_WITH_CHUNKS_SOURCES_SEPARATOR)
        );

        return new ChunkProvider()
            .setChunksBySource(filesWithChunksSources)
            .setRandomGenerator(randomGenerator);
    }

    private PasswordGenerator makePasswordGenerator(Map<String, String> environmentVariables,
        ChunkProvider chunkProvider
    ) {
        int passwordLength = Integer.parseInt(
            environmentVariables.get(
                PassGenerConstants.PASSWORD_LENGTH_ENV_VAR_NAME
            )
        );

        return new PasswordGenerator()
            .setPasswordLength(passwordLength)
            .setChunkProvider(chunkProvider);
    }

}
