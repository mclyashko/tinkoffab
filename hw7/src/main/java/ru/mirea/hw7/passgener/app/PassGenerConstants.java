package ru.mirea.hw7.passgener.app;

public class PassGenerConstants {

    public static final String SECURE_RANDOM_ALGORITHM = "SHA1PRNG";

    public static final String PASSWORD_LENGTH_ENV_VAR_NAME = "PASS_LENGTH";

    public static final String FILES_WITH_CHUNKS_SOURCES_ENV_VAR_NAME = "SRC";

    public static final String FILES_WITH_CHUNKS_SOURCES_SEPARATOR = ",";
}
