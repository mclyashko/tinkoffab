package ru.mirea.hw7.passgener.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.random.RandomGenerator;
import org.apache.commons.lang3.StringUtils;
import ru.mirea.hw7.passgener.util.resource.ResourceFileUtil;
import ru.mirea.hw7.passgener.util.stream.StreamUtil;

public class ChunkProvider {

    private int chunkSourceCounter = 0;
    private List<List<String>> chunksBySource;
    private RandomGenerator randomGenerator;

    public ChunkProvider setChunkSourceCounter(int chunkSourceCounter) {
        this.chunkSourceCounter = chunkSourceCounter;

        return this;
    }

    public ChunkProvider setChunksBySource(List<String> filesWithChunksSources) {
        chunksBySource = new ArrayList<>();

        for (String filePath : filesWithChunksSources) {
            chunksBySource.add(
                StreamUtil.linesFromInputStream(
                    ResourceFileUtil.getFileFromResourcesAsStream(filePath)
                )
            );
        }

        return this;
    }

    public ChunkProvider setRandomGenerator(RandomGenerator randomGenerator) {
        this.randomGenerator = randomGenerator;

        return this;
    }

    public String getRandomSuitableChunkFromNextSource(int maxChunkLength) {
        String chunk = getRandomSuitableChunkFromSource(chunksBySource.get(chunkSourceCounter),
            maxChunkLength);

        chunkSourceCounter = (chunkSourceCounter + 1) % chunksBySource.size();

        return StringUtils.capitalize(chunk);
    }

    private String getRandomSuitableChunkFromSource(List<String> source, int maxChunkLength) {
        List<String> filtered = filterChunksInSourceByLength(source, maxChunkLength);

        if (filtered.isEmpty()) {
            return "";
        }

        return filtered.get(randomGenerator.nextInt(filtered.size()));
    }

    private List<String> filterChunksInSourceByLength(List<String> source, int maxChunkLength) {
        List<String> filtered = new ArrayList<>();

        for (String chunk : source) {
            if (chunk.length() <= maxChunkLength) {
                filtered.add(chunk);
            }
        }

        return filtered;
    }
}
