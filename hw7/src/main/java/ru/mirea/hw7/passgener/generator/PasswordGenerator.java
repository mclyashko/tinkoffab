package ru.mirea.hw7.passgener.generator;

public class PasswordGenerator {

    private int passwordLength;
    private ChunkProvider chunkProvider;

    public PasswordGenerator setPasswordLength(int passwordLength) {
        this.passwordLength = passwordLength;

        return this;
    }

    public PasswordGenerator setChunkProvider(ChunkProvider chunkProvider) {
        this.chunkProvider = chunkProvider;

        return this;
    }

    public String generate() {
        StringBuilder passwordBuffer = new StringBuilder();

        fillWithRandomChunks(passwordBuffer, passwordLength);

        return passwordBuffer.toString();
    }

    private void fillWithRandomChunks(StringBuilder appendTo, int lengthToFill) {
        while (lengthToFill > 0) {
            String chunk = chunkProvider.getRandomSuitableChunkFromNextSource(lengthToFill);

            appendTo.append(chunk);

            lengthToFill -= chunk.length();
        }
    }
}
