package ru.mirea.hw7.passgener.util.resource;

import java.io.InputStream;

public class ResourceFileUtil {

    public static InputStream getFileFromResourcesAsStream(String fileName) {
        ClassLoader classLoader = ResourceFileUtil.class.getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream(fileName);

        if (inputStream == null) {
            throw new IllegalArgumentException("File with filename: '" + fileName + "' not found");
        } else {
            return inputStream;
        }
    }
}
