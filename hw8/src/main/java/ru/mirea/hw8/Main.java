package ru.mirea.hw8;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import reactor.core.publisher.Flux;

/**
 * В этой домашней работе вам необходимо найти в большом txt файле с книгами данные об авторах и
 * названиях произведений. Сделать это необходимо двумя способами: обычным последовательным и
 * реактивным, с использованием библиотеки reactor.core При этом нужно собрать статистику какой из
 * подходов более эффективен по расходу ресурсов. Советую обратить внимание на доклад:
 * <p><a href="https://www.youtube.com/watch?v=tjp8pTOyiWg">Максим Гореликов — Дизайн реактивной
 * системы на Spring 5/Reactor</a>
 * <p>Данные о задействованных ресурсах можно собрать к примеру с помощью {@link Runtime}.</p>
 * <p>Для RAM: {@link Runtime#totalMemory()} и {@link Runtime#freeMemory()}</p>
 */
public class Main {

    // Reactor Flux File Reading: https://www.vinsguru.com/reactor-flux-file-reading/

    private static final String FILE_PATH_ENV_VAR = "FILE_PATH";

    private static final Pattern TITLE_PATTERN = Pattern.compile("^Title: [\\w. ]*");

    private static final Pattern AUTHOR_PATTERN = Pattern.compile("^Author: [\\w. ]*");

    public static void main(String[] args) {
        String fileName = System.getenv(FILE_PATH_ENV_VAR);
        Path path = Paths.get(fileName);

        Instant start = Instant.now();

        reactiveVersion(path);

        Instant mid = Instant.now();

        listVersion(path);

        Instant end = Instant.now();

        System.out.println(Duration.between(start, mid));
        System.out.println(Duration.between(mid, end));
    }

    private static boolean validateLineWithPattern(String line, Pattern pattern) {
        return pattern.matcher(line).matches();
    }

    private static Flux<String> fluxFileLinesFromPath(Path path) {
        return Flux.using(
            () -> Files.lines(path), // Кажется, что эта операция неблокирующая, но получаю warning:
            // Possibly blocking call in non-blocking context could lead to thread starvation
            Flux::fromStream,
            Stream::close
        );
    }

    private static void reactiveVersion(Path path) {
        fluxFileLinesFromPath(path)
            .filter(
                (line) -> validateLineWithPattern(line, TITLE_PATTERN) ||
                    validateLineWithPattern(line, AUTHOR_PATTERN)
            )
            .subscribe(
                System.out::println
            );

        System.out.println(Runtime.getRuntime().totalMemory());
        System.out.println(Runtime.getRuntime().freeMemory());
    }

    private static void listVersion(Path path) {
        try (
            Stream<String> lines = Files.lines(path);
        ) {
            lines
                .filter((line) -> validateLineWithPattern(line, TITLE_PATTERN) ||
                    validateLineWithPattern(line, AUTHOR_PATTERN))
                .forEach(System.out::println);

            System.out.println(Runtime.getRuntime().totalMemory());
            System.out.println(Runtime.getRuntime().freeMemory());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}