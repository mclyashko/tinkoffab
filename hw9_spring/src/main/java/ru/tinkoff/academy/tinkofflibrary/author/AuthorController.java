package ru.tinkoff.academy.tinkofflibrary.author;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/authors")
@RequiredArgsConstructor
public class AuthorController {

    private final AuthorService authorService;

    @PostMapping
    public AuthorDto save(@RequestBody CreatingAuthorDto creatingAuthorDto) {
        return authorService.save(creatingAuthorDto);
    }

    @GetMapping
    public List<AuthorDto> getAll() {
        return authorService.findAll();
    }

    @GetMapping("/{authorId}")
    public ResponseEntity<AuthorDto> getById(@PathVariable Long authorId) {
        Optional<AuthorDto> author = authorService.findById(authorId);

        if (author.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(author.get(), HttpStatus.OK);
    }

    @DeleteMapping("/{authorId}")
    public ResponseEntity<AuthorDto> delete(@PathVariable Long authorId) {
        Optional<AuthorDto> author = authorService.removeById(authorId);

        if (author.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(author.get(), HttpStatus.OK);
    }
}
