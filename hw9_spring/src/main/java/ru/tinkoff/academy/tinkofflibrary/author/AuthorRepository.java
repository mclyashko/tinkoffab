package ru.tinkoff.academy.tinkofflibrary.author;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Repository;
import ru.tinkoff.academy.tinkofflibrary.common.AbstractRepository;

@Repository
public class AuthorRepository extends AbstractRepository<Author, Long> {

    @PostConstruct
    private void initDatabase() {
        database = new ArrayList<>(
            Arrays.asList(
                Author.builder().id(1L).name("В.О. Пелевин").build(),
                Author.builder().id(2L).name("В.В. Маяковский").build()
            )
        );
    }

    @Override
    public <S extends Author> S save(S entity) {
        final long RESERVED_ID = 0L;

        long generatedId =
            database.stream()
                .mapToLong(Author::getId)
                .max()
                .orElseGet(() -> RESERVED_ID) + 1;

        entity.setId(generatedId);

        database.add(entity);
        return entity;
    }

    @Override
    public List<Author> findAll() {
        return database;
    }

    @Override
    public Optional<Author> findById(Long authorId) {
        return database.stream()
            .filter(author -> author.getId().equals(authorId))
            .findAny();
    }

    @Override
    public Optional<Author> removeById(Long authorId) {
        Optional<Author> foundAuthor = findById(authorId);

        foundAuthor.ifPresent(foundAuthorValue -> database.remove(foundAuthorValue));

        return foundAuthor;
    }
}
