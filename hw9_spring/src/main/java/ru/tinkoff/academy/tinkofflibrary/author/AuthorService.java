package ru.tinkoff.academy.tinkofflibrary.author;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkofflibrary.book.BookService;

@Service
@RequiredArgsConstructor(onConstructor_ = {@Lazy})
public class AuthorService {

    private final BookService bookService;

    private final AuthorRepository authorRepository;

    public AuthorDto save(CreatingAuthorDto authorDto) {
        Author authorForSaving = Author.builder()
            .name(authorDto.getName()).build();

        return mapAuthorToReturningAuthorDto(authorRepository.save(authorForSaving));
    }

    public List<AuthorDto> findAll() {
        return authorRepository.findAll().stream()
            .map(this::mapAuthorToReturningAuthorDto)
            .toList();
    }

    public Optional<AuthorDto> findById(Long authorId) {
        return authorRepository
            .findById(authorId)
            .map(this::mapAuthorToReturningAuthorDto);
    }

    public Optional<AuthorDto> removeById(Long authorId) {
        return authorRepository
            .removeById(authorId)
            .map(this::mapAuthorToReturningAuthorDto);
    }

    private AuthorDto mapAuthorToReturningAuthorDto(Author author) {
        return AuthorDto.builder()
            .id(author.getId())
            .name(author.getName())
            .bookNames(bookService.findByAuthorId(author.getId())).build();
    }
}
