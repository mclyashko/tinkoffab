package ru.tinkoff.academy.tinkofflibrary.book;

import java.time.LocalDate;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Book {

    private Long id;
    private String name;
    private LocalDate publicationDate;
    private Long authorId;
    private Long genreId;
}
