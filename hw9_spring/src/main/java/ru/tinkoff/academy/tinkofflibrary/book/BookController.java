package ru.tinkoff.academy.tinkofflibrary.book;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    @PostMapping
    public BookDto save(@RequestBody CreatingBookDto creatingBookDto) {
        return bookService.save(creatingBookDto);
    }

    @GetMapping
    public List<BookDto> search(
        @RequestParam(name = "author", required = false) String authorName,
        @RequestParam(name = "genre", required = false) String genreName
    ) {
        return bookService.findFiltered(authorName, genreName);
    }

    @GetMapping("/{bookId}")
    public ResponseEntity<BookDto> getById(@PathVariable Long bookId) {
        Optional<BookDto> book = bookService.findById(bookId);

        if (book.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(book.get(), HttpStatus.OK);
    }

    @DeleteMapping("/{bookId}")
    public ResponseEntity<BookDto> delete(@PathVariable Long bookId) {
        Optional<BookDto> book = bookService.removeById(bookId);

        if (book.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(book.get(), HttpStatus.OK);
    }
}
