package ru.tinkoff.academy.tinkofflibrary.book;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BookDto {

    private Long id;
    private String name;
    private LocalDate publicationDate;
    private String authorName;
    private String genreName;
}
