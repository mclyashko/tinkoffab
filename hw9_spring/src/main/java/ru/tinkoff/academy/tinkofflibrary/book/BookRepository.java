package ru.tinkoff.academy.tinkofflibrary.book;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Repository;
import ru.tinkoff.academy.tinkofflibrary.common.AbstractRepository;

@Repository
public class BookRepository extends AbstractRepository<Book, Long> {

    @PostConstruct
    private void initDatabase() {
        database = new ArrayList<>(Arrays.asList(
            Book.builder()
                .id(1L)
                .name("Чапаев и Пустота")
                .publicationDate(LocalDate.of(1996, 10, 25))
                .authorId(1L)
                .genreId(1L).build(),
            Book.builder()
                .id(2L)
                .name("Клоп")
                .publicationDate(LocalDate.of(1929, 2, 13))
                .authorId(2L)
                .genreId(2L).build())
        );
    }

    @Override
    public <S extends Book> S save(S entity) {
        final long RESERVED_ID = 0L;

        long generatedId = database.stream()
            .mapToLong(Book::getId)
            .max()
            .orElse(RESERVED_ID) + 1;

        entity.setId(generatedId);

        database.add(entity);
        return entity;
    }

    @Override
    public List<Book> findAll() {
        return database;
    }

    @Override
    public Optional<Book> findById(Long bookId) {
        return database.stream()
            .filter(book -> book.getId().equals(bookId))
            .findAny();
    }

    public List<Book> findByAuthorId(Long authorId) {
        return database.stream()
            .filter(book -> book.getAuthorId().equals(authorId))
            .toList();
    }

    public List<Book> findByGenreId(Long genreId) {
        return database.stream()
            .filter(book -> book.getGenreId().equals(genreId))
            .toList();
    }

    @Override
    public Optional<Book> removeById(Long bookId) {
        Optional<Book> foundBook = findById(bookId);

        foundBook.ifPresent(
            foundBookValue -> database.remove(foundBookValue)
        );

        return foundBook;
    }
}
