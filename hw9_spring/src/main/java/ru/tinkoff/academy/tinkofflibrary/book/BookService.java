package ru.tinkoff.academy.tinkofflibrary.book;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkofflibrary.author.AuthorService;
import ru.tinkoff.academy.tinkofflibrary.author.AuthorDto;
import ru.tinkoff.academy.tinkofflibrary.genre.GenreService;
import ru.tinkoff.academy.tinkofflibrary.genre.ReturningGenreDto;

@Service
@RequiredArgsConstructor(onConstructor_ = {@Lazy})
public class BookService {

    private final AuthorService authorService;
    private final GenreService genreService;

    private final BookRepository bookRepository;

    public BookDto save(CreatingBookDto bookDto) {
        Book bookForSaving = Book.builder()
            .name(bookDto.getName())
            .publicationDate(bookDto.getPublicationDate())
            .authorId(bookDto.getAuthorId())
            .genreId(bookDto.getGenreId()).build();

        return mapBookToReturningBookDto(bookRepository.save(bookForSaving));
    }

    public List<BookDto> findAll() {
        return bookRepository.findAll().stream()
            .map(this::mapBookToReturningBookDto)
            .toList();
    }

    public List<BookDto> findFiltered(
        String authorName,
        String genreName
    ) {
        List<BookDto> books = findAll();

        books = authorName != null ?
            books.stream().filter(book -> book.getAuthorName().equals(authorName)).toList() :
            books;

        books = genreName != null ?
            books.stream().filter(book -> book.getGenreName().equals(genreName)).toList() :
            books;

        return books;
    }

    public Optional<BookDto> findById(Long bookId) {
        return bookRepository.findById(bookId).
            map(this::mapBookToReturningBookDto);
    }

    public Optional<BookDto> removeById(Long bookId) {
        return bookRepository.removeById(bookId)
            .map(this::mapBookToReturningBookDto);
    }

    public List<String> findByAuthorId(Long authorId) {
        return bookRepository.findByAuthorId(authorId).stream()
            .map(Book::getName)
            .toList();
    }

    public List<String> findByGenreId(Long genreId) {
        return bookRepository.findByGenreId(genreId).stream()
            .map(Book::getName)
            .toList();
    }

    private BookDto mapBookToReturningBookDto(Book book) {
        Optional<AuthorDto> author = authorService.findById(book.getAuthorId());
        Optional<ReturningGenreDto> genre = genreService.findById(book.getGenreId());

        String authorName = author.map(AuthorDto::getName).orElse(null);
        String genreName = genre.map(ReturningGenreDto::getName).orElse(null);

        return BookDto.builder()
            .id(book.getId())
            .name(book.getName())
            .publicationDate(book.getPublicationDate())
            .authorName(authorName)
            .genreName(genreName).build();
    }
}
