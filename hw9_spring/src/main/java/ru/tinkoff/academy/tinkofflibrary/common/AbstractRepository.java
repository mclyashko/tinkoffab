package ru.tinkoff.academy.tinkofflibrary.common;

import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<T, ID> {

    protected List<T> database;

    public abstract <S extends T> S save(S entity);

    public abstract List<T> findAll();

    public abstract Optional<T> findById(ID id);

    public abstract Optional<T> removeById(ID id);

}
