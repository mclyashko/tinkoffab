package ru.tinkoff.academy.tinkofflibrary.genre;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/genres")
@RequiredArgsConstructor
public class GenreController {

    private final GenreService genreService;

    @PostMapping
    public ReturningGenreDto save(@RequestBody GenreDto genreDto) {
        return genreService.save(genreDto);
    }

    @GetMapping
    public List<ReturningGenreDto> getAll() {
        return genreService.findAll();
    }

    @GetMapping("/{genreId}")
    public ResponseEntity<ReturningGenreDto> getById(@PathVariable Long genreId) {
        Optional<ReturningGenreDto> genre = genreService.findById(genreId);

        if (genre.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(genre.get(), HttpStatus.OK);
    }

    @DeleteMapping("/{genreId}")
    public ResponseEntity<ReturningGenreDto> delete(@PathVariable Long genreId) {
        Optional<ReturningGenreDto> author = genreService.removeById(genreId);

        if (author.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(author.get(), HttpStatus.OK);
    }
}
