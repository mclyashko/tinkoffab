package ru.tinkoff.academy.tinkofflibrary.genre;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Repository;
import ru.tinkoff.academy.tinkofflibrary.common.AbstractRepository;

@Repository
public class GenreRepository extends AbstractRepository<Genre, Long> {

    @PostConstruct
    private void initDatabase() {
        database = new ArrayList<>(
            Arrays.asList(
                Genre.builder().id(1L).name("Роман").build(),
                Genre.builder().id(2L).name("Пьеса").build()
            )
        );
    }

    @Override
    public <S extends Genre> S save(S entity) {
        final long RESERVED_ID = 0L;

        long generatedId = database.stream()
            .mapToLong(Genre::getId)
            .max()
            .orElse(RESERVED_ID) + 1;

        entity.setId(generatedId);

        database.add(entity);
        return entity;
    }

    @Override
    public List<Genre> findAll() {
        return database;
    }

    @Override
    public Optional<Genre> findById(Long genreId) {
        return database.stream()
            .filter(genre -> genre.getId().equals(genreId))
            .findAny();
    }

    @Override
    public Optional<Genre> removeById(Long genreId) {
        Optional<Genre> foundGenre = findById(genreId);

        foundGenre.ifPresent(
            foundGenreValue -> database.remove(foundGenreValue)
        );

        return foundGenre;
    }
}
