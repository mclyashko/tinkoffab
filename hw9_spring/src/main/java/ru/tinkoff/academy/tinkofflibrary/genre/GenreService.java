package ru.tinkoff.academy.tinkofflibrary.genre;

import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkofflibrary.book.BookService;

@Service
@RequiredArgsConstructor(onConstructor_ = {@Lazy})
public class GenreService {

    private final BookService bookService;

    private final GenreRepository genreRepository;

    public ReturningGenreDto save(GenreDto genreDto) {
        Genre genreForSaving = Genre.builder()
            .name(genreDto.getName()).build();

        return mapGenreToReturningGenreDto(genreRepository.save(genreForSaving));
    }

    public List<ReturningGenreDto> findAll() {
        return genreRepository.findAll().stream()
            .map(this::mapGenreToReturningGenreDto)
            .toList();
    }

    public Optional<ReturningGenreDto> findById(Long genreId) {
        return genreRepository.findById(genreId)
            .map(this::mapGenreToReturningGenreDto);
    }

    public Optional<ReturningGenreDto> removeById(Long genreId) {
        return genreRepository.removeById(genreId)
            .map(this::mapGenreToReturningGenreDto);
    }

    private ReturningGenreDto mapGenreToReturningGenreDto(Genre genre) {
        return ReturningGenreDto.builder()
            .id(genre.getId())
            .name(genre.getName())
            .bookNames(bookService.findByGenreId(genre.getId())).build();
    }
}
