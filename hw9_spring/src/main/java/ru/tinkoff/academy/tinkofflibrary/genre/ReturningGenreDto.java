package ru.tinkoff.academy.tinkofflibrary.genre;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReturningGenreDto {

    private Long id;
    private String name;
    private List<String> bookNames;
}
