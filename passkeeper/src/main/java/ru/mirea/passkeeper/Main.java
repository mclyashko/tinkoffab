package ru.mirea.passkeeper;

import ru.mirea.passkeeper.crypto.Decryptor;
import ru.mirea.passkeeper.crypto.Encryptor;
import ru.mirea.passkeeper.crypto.cryptor.builder.DecryptorBuilder;
import ru.mirea.passkeeper.crypto.cryptor.builder.EncryptionType;
import ru.mirea.passkeeper.crypto.cryptor.builder.EncryptorBuilder;
import ru.mirea.passkeeper.io.Readable;
import ru.mirea.passkeeper.io.Writable;
import ru.mirea.passkeeper.io.file.ReadableFile;
import ru.mirea.passkeeper.io.file.WritableFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    /**
     * Этот страшный код будет переработан. Используется только для демонстрации.
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("in and out paths: ");
        String inputPath, outputPath;
        inputPath = scanner.nextLine();
        outputPath = scanner.nextLine();
        Readable readable = new ReadableFile(inputPath);
        Writable writable = new WritableFile(outputPath);

        System.out.println("key: ");
        String key;
        key = scanner.nextLine();

        System.out.println("1 - AES, 2 - Blowfish, 3 - Rc4");
        int num = Integer.parseInt(scanner.nextLine());

        System.out.println("en or de? ");
        String enOrDe = scanner.nextLine();

        if (enOrDe.equals("en")) {
            EncryptorBuilder encryptorBuilder = new EncryptorBuilder();
            encryptorBuilder.setInitialKey(key);

            switch (num) {
                case 1 -> encryptorBuilder.setType(EncryptionType.AES).build();
                case 2 -> encryptorBuilder.setType(EncryptionType.BLOWFISH).build();
                case 3 -> encryptorBuilder.setType(EncryptionType.RC_4).build();
            }

            Encryptor encryptor = encryptorBuilder.build();

            List<String> in = new ArrayList<>();
            readable.read(in);
            String out;

            out = encryptor.encrypt(String.join("\n", in));

            writable.write(List.of(out));
        } else {
            DecryptorBuilder decryptorBuilder = new DecryptorBuilder();
            decryptorBuilder.setInitialKey(key);

            switch (num) {
                case 1 -> decryptorBuilder.setType(EncryptionType.AES).build();
                case 2 -> decryptorBuilder.setType(EncryptionType.BLOWFISH).build();
                case 3 -> decryptorBuilder.setType(EncryptionType.RC_4).build();
            }

            Decryptor decryptor = decryptorBuilder.build();

            List<String> in = new ArrayList<>();
            readable.read(in);
            String out;

            out = decryptor.decrypt(String.join("\n", in));

            writable.write(List.of(out));
        }
    }
}
