package ru.mirea.passkeeper.crypto;

public interface Decryptor {
    String decrypt(String cipherText);
}
