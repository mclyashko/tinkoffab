package ru.mirea.passkeeper.crypto;

public interface Encryptor {
    String encrypt(String plainText);
}
