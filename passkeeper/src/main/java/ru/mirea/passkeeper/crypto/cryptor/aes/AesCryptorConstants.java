package ru.mirea.passkeeper.crypto.cryptor.aes;

public final class AesCryptorConstants {
    public static final String KEY_ALGORITHM = "AES";

    public static final String CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding";
}
