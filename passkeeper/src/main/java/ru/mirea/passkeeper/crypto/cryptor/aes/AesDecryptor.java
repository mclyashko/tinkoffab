package ru.mirea.passkeeper.crypto.cryptor.aes;

import ru.mirea.passkeeper.crypto.Decryptor;
import ru.mirea.passkeeper.crypto.cryptor.basic.AbstractDecryptor;

public final class AesDecryptor extends AbstractDecryptor implements Decryptor {
    public AesDecryptor(String initialKey) {
        super(initialKey, AesCryptorConstants.KEY_ALGORITHM, AesCryptorConstants.CIPHER_ALGORITHM);
    }

}
