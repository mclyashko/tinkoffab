package ru.mirea.passkeeper.crypto.cryptor.aes;

import ru.mirea.passkeeper.crypto.Encryptor;
import ru.mirea.passkeeper.crypto.cryptor.basic.AbstractEncryptor;

public final class AesEncryptor extends AbstractEncryptor implements Encryptor {
    public AesEncryptor(String initialKey) {
        super(initialKey, AesCryptorConstants.KEY_ALGORITHM, AesCryptorConstants.CIPHER_ALGORITHM);
    }
}
