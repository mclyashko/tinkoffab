package ru.mirea.passkeeper.crypto.cryptor.basic;

import ru.mirea.passkeeper.util.keygenerator.KeyGeneratorGetter;
import ru.mirea.passkeeper.util.securerandom.SecureRandomGetter;

import javax.crypto.KeyGenerator;
import java.security.Key;
import java.security.SecureRandom;

abstract class AbstractCryptor {
    private static final String SECURE_RANDOM_ALGORITHM = "SHA1PRNG";

    private static final int REAL_KEY_SIZE = 256;

    protected static final String CHARSET_NAME = "UTF-8";

    protected final String cipherAlgorithm;
    protected final Key realKey;

    protected AbstractCryptor(String initialKey, String keyAlgorithm, String cipherAlgorithm) {
        this.realKey = getRealKey(initialKey, keyAlgorithm);
        this.cipherAlgorithm = cipherAlgorithm;
    }

    private Key getRealKey(String initialKey, String keyAlgorithm) {
        KeyGenerator keyGen;
        SecureRandom secureRandom;

        keyGen = KeyGeneratorGetter.getInstance(keyAlgorithm);
        secureRandom = SecureRandomGetter.getInstance(SECURE_RANDOM_ALGORITHM);

        secureRandom.setSeed(initialKey.getBytes());
        keyGen.init(REAL_KEY_SIZE, secureRandom);

        return keyGen.generateKey();
    }
}
