package ru.mirea.passkeeper.crypto.cryptor.basic;

import ru.mirea.passkeeper.crypto.Decryptor;
import ru.mirea.passkeeper.util.cipher.Cipher;
import ru.mirea.passkeeper.util.string.StringUtil;

import java.util.Base64;

public abstract class AbstractDecryptor extends AbstractCryptor implements Decryptor {
    protected AbstractDecryptor(String initialKey, String keyAlgorithm, String cipherAlgorithm) {
        super(initialKey, keyAlgorithm, cipherAlgorithm);
    }

    @Override
    public String decrypt(String cipherText) {
        byte[] cipherBytes = Base64.getDecoder().decode(cipherText);

        Cipher cipher = Cipher.getInstance(cipherAlgorithm);
        cipher.init(Cipher.DECRYPT_MODE, realKey);

        byte[] plainBytes = cipher.doFinal(cipherBytes);

        return StringUtil.getStringFromBytes(plainBytes, CHARSET_NAME);
    }
}
