package ru.mirea.passkeeper.crypto.cryptor.basic;

import ru.mirea.passkeeper.crypto.Encryptor;
import ru.mirea.passkeeper.util.cipher.Cipher;
import ru.mirea.passkeeper.util.string.StringUtil;

import java.util.Base64;

public abstract class AbstractEncryptor extends AbstractCryptor implements Encryptor {
    protected AbstractEncryptor(String initialKey, String keyAlgorithm, String cipherAlgorithm) {
        super(initialKey, keyAlgorithm, cipherAlgorithm);
    }

    @Override
    public String encrypt(String plainText) {

        byte[] plainBytes = StringUtil.getBytesFromString(plainText, CHARSET_NAME);

        Cipher cipher = Cipher.getInstance(cipherAlgorithm);
        cipher.init(Cipher.ENCRYPT_MODE, realKey);

        byte[] cipherBytes = cipher.doFinal(plainBytes);

        return Base64.getEncoder().encodeToString(cipherBytes);
    }
}
