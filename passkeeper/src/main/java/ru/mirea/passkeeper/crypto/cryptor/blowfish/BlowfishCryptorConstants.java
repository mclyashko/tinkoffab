package ru.mirea.passkeeper.crypto.cryptor.blowfish;

public final class BlowfishCryptorConstants {
    public static final String KEY_ALGORITHM = "Blowfish";

    public static final String CIPHER_ALGORITHM = "Blowfish";
}
