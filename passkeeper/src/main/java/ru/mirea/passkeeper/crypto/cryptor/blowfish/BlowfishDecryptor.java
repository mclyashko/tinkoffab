package ru.mirea.passkeeper.crypto.cryptor.blowfish;

import ru.mirea.passkeeper.crypto.Decryptor;
import ru.mirea.passkeeper.crypto.cryptor.basic.AbstractDecryptor;

public final class BlowfishDecryptor extends AbstractDecryptor implements Decryptor {
    public BlowfishDecryptor(String initialKey) {
        super(initialKey, BlowfishCryptorConstants.KEY_ALGORITHM, BlowfishCryptorConstants.CIPHER_ALGORITHM);
    }
}
