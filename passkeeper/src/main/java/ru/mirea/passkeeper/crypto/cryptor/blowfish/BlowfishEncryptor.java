package ru.mirea.passkeeper.crypto.cryptor.blowfish;

import ru.mirea.passkeeper.crypto.Encryptor;
import ru.mirea.passkeeper.crypto.cryptor.basic.AbstractEncryptor;

public final class BlowfishEncryptor extends AbstractEncryptor implements Encryptor {
    public BlowfishEncryptor(String initialKey) {
        super(initialKey, BlowfishCryptorConstants.KEY_ALGORITHM, BlowfishCryptorConstants.CIPHER_ALGORITHM);
    }
}
