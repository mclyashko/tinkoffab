package ru.mirea.passkeeper.crypto.cryptor.builder;

import ru.mirea.passkeeper.crypto.Decryptor;
import ru.mirea.passkeeper.crypto.cryptor.aes.AesDecryptor;
import ru.mirea.passkeeper.crypto.cryptor.blowfish.BlowfishDecryptor;
import ru.mirea.passkeeper.crypto.cryptor.rc4.Rc4Decryptor;

public final class DecryptorBuilder {

    private EncryptionType encryptionType;

    private String initialKey;

    public DecryptorBuilder setType(EncryptionType encryptionType) {
        this.encryptionType = encryptionType;

        return this;
    }

    public DecryptorBuilder setInitialKey(String initialKey) {
        this.initialKey = initialKey;

        return this;
    }
    public Decryptor build() {
        return switch (encryptionType){
            case AES -> new AesDecryptor(initialKey);
            case RC_4 -> new Rc4Decryptor(initialKey);
            case BLOWFISH -> new BlowfishDecryptor(initialKey);
        };
    }
}
