package ru.mirea.passkeeper.crypto.cryptor.builder;

public enum EncryptionType {
    AES, RC_4, BLOWFISH
}
