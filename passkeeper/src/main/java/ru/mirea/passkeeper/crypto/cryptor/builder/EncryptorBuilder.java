package ru.mirea.passkeeper.crypto.cryptor.builder;

import ru.mirea.passkeeper.crypto.Encryptor;
import ru.mirea.passkeeper.crypto.cryptor.aes.AesEncryptor;
import ru.mirea.passkeeper.crypto.cryptor.blowfish.BlowfishEncryptor;
import ru.mirea.passkeeper.crypto.cryptor.rc4.Rc4Encryptor;

public final class EncryptorBuilder {

    private EncryptionType encryptionType;

    private String initialKey;

    public EncryptorBuilder setType(EncryptionType encryptionType) {
        this.encryptionType = encryptionType;

        return this;
    }

    public EncryptorBuilder setInitialKey(String initialKey) {
        this.initialKey = initialKey;

        return this;
    }

    public Encryptor build() {
        return switch (encryptionType) {
            case AES -> new AesEncryptor(initialKey);
            case RC_4 -> new Rc4Encryptor(initialKey);
            case BLOWFISH -> new BlowfishEncryptor(initialKey);
        };
    }
}
