package ru.mirea.passkeeper.crypto.cryptor.rc4;

public final class Rc4CryptorConstants {
    public static final String KEY_ALGORITHM = "RC4";

    public static final String CIPHER_ALGORITHM = "RC4";
}
