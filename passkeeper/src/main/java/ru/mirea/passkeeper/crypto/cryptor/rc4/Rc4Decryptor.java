package ru.mirea.passkeeper.crypto.cryptor.rc4;

import ru.mirea.passkeeper.crypto.Decryptor;
import ru.mirea.passkeeper.crypto.cryptor.basic.AbstractDecryptor;

public final class Rc4Decryptor extends AbstractDecryptor implements Decryptor {
    public Rc4Decryptor(String initialKey) {
        super(initialKey, Rc4CryptorConstants.KEY_ALGORITHM, Rc4CryptorConstants.CIPHER_ALGORITHM);
    }
}
