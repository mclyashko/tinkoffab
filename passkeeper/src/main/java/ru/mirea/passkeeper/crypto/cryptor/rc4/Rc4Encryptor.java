package ru.mirea.passkeeper.crypto.cryptor.rc4;

import ru.mirea.passkeeper.crypto.Encryptor;
import ru.mirea.passkeeper.crypto.cryptor.basic.AbstractEncryptor;

public final class Rc4Encryptor extends AbstractEncryptor implements Encryptor {

    public Rc4Encryptor(String initialKey) {
        super(initialKey, Rc4CryptorConstants.KEY_ALGORITHM, Rc4CryptorConstants.CIPHER_ALGORITHM);
    }
}
