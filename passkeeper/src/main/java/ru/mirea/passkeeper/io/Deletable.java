package ru.mirea.passkeeper.io;

public interface Deletable {
    /**
     * Метод, удаляющий ресурс.
     * Возвращает true в случае успешного удаления,
     * false в случае отсутствия
     * Вызывает ошибку (RuntimeException(IOException)) в случае невозможности удаления.
     */
    boolean delete();
}
