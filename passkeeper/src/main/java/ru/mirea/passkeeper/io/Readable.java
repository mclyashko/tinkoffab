package ru.mirea.passkeeper.io;

import java.util.List;

public interface Readable {
    /**
     * Метод, считывающий данные ресурса в buffer,
     * возвращающий количество считанных строк.
     * Вызывает ошибку (RuntimeException(IOException)) в случае невозможности чтения.
     */
    int read(List<String> buffer);
}
