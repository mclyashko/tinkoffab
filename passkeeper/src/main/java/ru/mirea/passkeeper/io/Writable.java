package ru.mirea.passkeeper.io;

import java.util.List;

public interface Writable {
    /**
     * Метод, пишущий данные в ресурс,
     * возвращающий количество записанных строк.
     * Вызывает ошибку (RuntimeException(IOException)) в случае невозможности записи.
     */
    int write(List<String> buffer);
}
