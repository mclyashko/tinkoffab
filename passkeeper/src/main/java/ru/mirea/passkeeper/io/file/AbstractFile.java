package ru.mirea.passkeeper.io.file;

import java.nio.file.Path;
import java.nio.file.Paths;

abstract class AbstractFile {
    protected final Path filePath;

    protected AbstractFile(String path) {
        filePath = Paths.get(path).normalize();
    }
}
