package ru.mirea.passkeeper.io.file;

import ru.mirea.passkeeper.io.Deletable;

import java.io.IOException;
import java.nio.file.Files;

public class DeletableFile extends AbstractFile implements Deletable {
    public DeletableFile(String path) {
        super(path);
    }

    @Override
    public boolean delete() {
        try {
            return Files.deleteIfExists(filePath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
