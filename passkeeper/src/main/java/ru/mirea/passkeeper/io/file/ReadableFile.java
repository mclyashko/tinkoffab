package ru.mirea.passkeeper.io.file;

import ru.mirea.passkeeper.io.Readable;

import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class ReadableFile extends AbstractFile implements Readable {
    public ReadableFile(String path) {
        super(path);
    }

    @Override
    public int read(List<String> buffer) {
        buffer.clear();

        try {
            buffer.addAll(Files.readAllLines(filePath));
            return buffer.size();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
