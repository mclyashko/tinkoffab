package ru.mirea.passkeeper.io.file;

import ru.mirea.passkeeper.io.Writable;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class WritableFile extends AbstractFile implements Writable {
    public WritableFile(String path) {
        super(path);
    }

    @Override
    public int write(List<String> buffer) {
        try {
            Files.write(filePath, buffer, StandardOpenOption.CREATE);
            return buffer.size();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
