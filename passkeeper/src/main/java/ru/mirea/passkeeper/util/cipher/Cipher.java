package ru.mirea.passkeeper.util.cipher;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public final class Cipher {

    public static final int ENCRYPT_MODE = javax.crypto.Cipher.ENCRYPT_MODE;
    public static final int DECRYPT_MODE = javax.crypto.Cipher.DECRYPT_MODE;
    private final javax.crypto.Cipher cipher;

    private Cipher(String transformation) {
        try {
            cipher = javax.crypto.Cipher.getInstance(transformation);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(
                    "No such transformation algorithm: '" + transformation + "' for cipher",
                    e
            );
        } catch (NoSuchPaddingException e) {
            throw new RuntimeException(
                    "No such padding for transformation algorithm: '" + transformation + "' for cipher",
                    e
            );
        }
    }

    public static Cipher getInstance(String transformation) {
        return new Cipher(transformation);
    }

    public void init(int mode, Key key) {
        try {
            cipher.init(mode, key);
        } catch (InvalidKeyException e) {
            throw new RuntimeException(
                    "No installed provider supports this key: '" + key + "'; " +
                            "cipher mode: '" + mode + "'",
                    e
            );
        }
    }

    public byte[] doFinal(byte[] input) {
        try {
            return cipher.doFinal(input);
        } catch (IllegalBlockSizeException e) {
            throw new RuntimeException(
                    "Illegal block size exception. cipher: '" + cipher + "'; " +
                            "byte[] input: '" + Arrays.toString(input) + "'; " +
                            "input length: '" + input.length + "'",
                    e
            );
        } catch (BadPaddingException e) {
            throw new RuntimeException(
                    "Bad padding exception. cipher: '" + cipher + "'; " +
                            "byte[] input: '" + Arrays.toString(input) + "'; " +
                            "input length: '" + input.length + "'",
                    e
            );
        }
    }
}
