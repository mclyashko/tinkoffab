package ru.mirea.passkeeper.util.keygenerator;

import javax.crypto.KeyGenerator;
import java.security.NoSuchAlgorithmException;

public final class KeyGeneratorGetter {
    public static KeyGenerator getInstance(String algorithm) {
        try {
            return KeyGenerator.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(
                    "KeyGenerator not available for algorithm: " + algorithm,
                    e
            );
        }
    }
}
