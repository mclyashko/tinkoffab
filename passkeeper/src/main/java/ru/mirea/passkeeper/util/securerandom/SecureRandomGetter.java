package ru.mirea.passkeeper.util.securerandom;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public final class SecureRandomGetter {
    public static SecureRandom getInstance(String algorithm) {
        try {
            return SecureRandom.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(
                    "SecureRandom not available for algorithm: " + algorithm,
                    e
            );
        }
    }
}
