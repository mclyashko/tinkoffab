package ru.mirea.passkeeper.util.string;

import java.io.UnsupportedEncodingException;

public class StringUtil {
    public static byte[] getBytesFromString(String string, String charsetName) {
        try {
            return string.getBytes(charsetName);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(
                    "Unsupported charset: '" + charsetName + "'",
                    e
            );
        }
    }

    public static String getStringFromBytes(byte[] bytes, String charsetName) {
        try {
            return new String(bytes, charsetName);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(
                    "Unsupported charset: '" + charsetName + "'",
                    e
            );
        }
    }
}
