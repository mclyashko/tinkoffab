package ru.mirea.passkeeper.crypto.cryptor.aes;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.mirea.passkeeper.crypto.Decryptor;

class AesDecryptorTest {

    private static final String FIRST_INIT_PASSWORD = "1234567890";

    private static final String SECOND_INIT_PASSWORD = "Lorem ipsum dolor sit amet.";

    private static final String STRING_TO_ENCRYPT = "Hello, reĝiĉo, übrigens, КПСС, ジョジョの奇妙な冒険";

    private static final String ENCRYPTED_STRING_WITH_FIRST_INIT_PASSWORD = "xdl2U2omj/VoKxgs388gY5lob5XNh9yMguw+END+Na5/OxtxSY55HgXlVdXa9vt/lFifK+rDymFRomiVqSchtMGu/VfXQL+/teMR9E4uTNs=";

    private static final String ENCRYPTED_STRING_WITH_SECOND_INIT_PASSWORD = "dMWvMSf9OzzT+bh6yo6nLQRJsoo9JO23qGfsv1FgIyTFR4hmRI/+w8Cl89j6Av1HNG/ca2IulwxMl5dJiTC5BFrIdEAmpZEHpUjK03c6yIc=";

    Decryptor aesDecryptor;

    @Test
    void decryptWithFirstInitPassword() {
        aesDecryptor = new AesDecryptor(FIRST_INIT_PASSWORD);

        String decrypted = aesDecryptor.decrypt(ENCRYPTED_STRING_WITH_FIRST_INIT_PASSWORD);

        Assertions.assertEquals(STRING_TO_ENCRYPT, decrypted);
    }

    @Test
    void decryptWithSecondInitPassword() {
        aesDecryptor = new AesDecryptor(SECOND_INIT_PASSWORD);

        String decrypted = aesDecryptor.decrypt(ENCRYPTED_STRING_WITH_SECOND_INIT_PASSWORD);

        Assertions.assertEquals(STRING_TO_ENCRYPT, decrypted);
    }
}