package ru.mirea.passkeeper.crypto.cryptor.blowfish;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.mirea.passkeeper.crypto.Decryptor;

class BlowfishDecryptorTest {

    private static final String FIRST_INIT_PASSWORD = "1234567890";

    private static final String SECOND_INIT_PASSWORD = "Lorem ipsum dolor sit amet.";

    private static final String STRING_TO_ENCRYPT = "Hello, reĝiĉo, übrigens, КПСС, ジョジョの奇妙な冒険";

    private static final String ENCRYPTED_STRING_WITH_FIRST_INIT_PASSWORD = "leZ5YfMSrojhRRnyE2OkjePvZycL0bYhuGdTXXABU6pfmJ1E8nCHTLLx0lM1QHzB3YsvUOb7Paxq4+CtgVjBAFgzQ36OWo/Y";

    private static final String ENCRYPTED_STRING_WITH_SECOND_INIT_PASSWORD = "a1DfYeScEwcAn7eGqPsSh+VEcKBSVgKBr2IKp2IA4tRBxcOKdkfIgelgz5IUBdd+IRLbAPSlCUg8PcTicd1nQyspafw+2nUR";

    Decryptor blowfishDecryptor;

    @Test
    void decryptWithFirstInitPassword() {
        blowfishDecryptor = new BlowfishDecryptor(FIRST_INIT_PASSWORD);

        String decrypted = blowfishDecryptor.decrypt(ENCRYPTED_STRING_WITH_FIRST_INIT_PASSWORD);

        Assertions.assertEquals(STRING_TO_ENCRYPT, decrypted);
    }

    @Test
    void decryptWithSecondInitPassword() {
        blowfishDecryptor = new BlowfishDecryptor(SECOND_INIT_PASSWORD);

        String decrypted = blowfishDecryptor.decrypt(ENCRYPTED_STRING_WITH_SECOND_INIT_PASSWORD);

        Assertions.assertEquals(STRING_TO_ENCRYPT, decrypted);
    }
}