package ru.mirea.passkeeper.crypto.cryptor.builder;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.mirea.passkeeper.crypto.Decryptor;
import ru.mirea.passkeeper.crypto.cryptor.aes.AesDecryptor;

class DecryptorBuilderTest {
    private static final String INIT_PASSWORD = "1234567890";

    private static final String STRING_TO_DECRYPT = "xdl2U2omj/VoKxgs388gY5lob5XNh9yMguw+END+Na5/OxtxSY55HgXlVdXa9vt/lFifK+rDymFRomiVqSchtMGu/VfXQL+/teMR9E4uTNs=";

    private static final String DECRYPTED_STRING_WITH_AES = "Hello, reĝiĉo, übrigens, КПСС, ジョジョの奇妙な冒険";

    DecryptorBuilder decryptorBuilder;

    @Test
    void buildAesDecryptorCheckType() {
        decryptorBuilder = new DecryptorBuilder();

        Decryptor aesEncryptor = decryptorBuilder.
                setType(EncryptionType.AES).
                setInitialKey(INIT_PASSWORD).
                build();

        Assertions.assertInstanceOf(AesDecryptor.class, aesEncryptor);
    }

    @Test
    void buildAesDecryptorCheckDecryption() {
        decryptorBuilder = new DecryptorBuilder();

        Decryptor aesEncryptor = decryptorBuilder.
                setType(EncryptionType.AES).
                setInitialKey(INIT_PASSWORD).
                build();

        String encrypted = aesEncryptor.decrypt(STRING_TO_DECRYPT);

        Assertions.assertEquals(DECRYPTED_STRING_WITH_AES, encrypted);
    }
}