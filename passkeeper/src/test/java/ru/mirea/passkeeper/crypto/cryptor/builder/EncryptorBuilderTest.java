package ru.mirea.passkeeper.crypto.cryptor.builder;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.mirea.passkeeper.crypto.Encryptor;
import ru.mirea.passkeeper.crypto.cryptor.aes.AesEncryptor;

class EncryptorBuilderTest {
    private static final String INIT_PASSWORD = "1234567890";

    private static final String STRING_TO_ENCRYPT = "Hello, reĝiĉo, übrigens, КПСС, ジョジョの奇妙な冒険";

    private static final String ENCRYPTED_STRING_WITH_AES = "xdl2U2omj/VoKxgs388gY5lob5XNh9yMguw+END+Na5/OxtxSY55HgXlVdXa9vt/lFifK+rDymFRomiVqSchtMGu/VfXQL+/teMR9E4uTNs=";

    EncryptorBuilder encryptorBuilder;

    @Test
    void buildAesEncryptorCheckType() {
        encryptorBuilder = new EncryptorBuilder();

        Encryptor aesEncryptor = encryptorBuilder.
                setType(EncryptionType.AES).
                setInitialKey(INIT_PASSWORD).
                build();

        Assertions.assertInstanceOf(AesEncryptor.class, aesEncryptor);
    }

    @Test
    void buildAesEncryptorCheckEncryption() {
        encryptorBuilder = new EncryptorBuilder();

        Encryptor aesEncryptor = encryptorBuilder.
                setType(EncryptionType.AES).
                setInitialKey(INIT_PASSWORD).
                build();

        String encrypted = aesEncryptor.encrypt(STRING_TO_ENCRYPT);

        Assertions.assertEquals(ENCRYPTED_STRING_WITH_AES, encrypted);
    }
}