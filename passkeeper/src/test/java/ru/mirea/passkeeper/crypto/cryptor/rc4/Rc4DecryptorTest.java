package ru.mirea.passkeeper.crypto.cryptor.rc4;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.mirea.passkeeper.crypto.Decryptor;

class Rc4DecryptorTest {

    private static final String FIRST_INIT_PASSWORD = "1234567890";

    private static final String SECOND_INIT_PASSWORD = "Lorem ipsum dolor sit amet.";

    private static final String STRING_TO_ENCRYPT = "Hello, reĝiĉo, übrigens, КПСС, ジョジョの奇妙な冒険";

    private static final String ENCRYPTED_STRING_WITH_FIRST_INIT_PASSWORD = "k5ZYhe9XW0AI8CzyzQkUonvhBLQEaBYGNHKnMNFUs85A0x4B+cVj5NP3++kDGCVpDtq7e4QAilUtb3wGyTFIOiRVshE=";

    private static final String ENCRYPTED_STRING_WITH_SECOND_INIT_PASSWORD = "Olsar8o192/cBKQXUscNLvIP8GZI5bhKerw+c84KbDQDM1ccDGOs3m8ye5/9i3vmXl00WqR2P4z9w6Kq8BPLWOpotSM=";

    Decryptor rc4Decryptor;

    @Test
    void decryptWithFirstInitPassword() {
        rc4Decryptor = new Rc4Decryptor(FIRST_INIT_PASSWORD);

        String decrypted = rc4Decryptor.decrypt(ENCRYPTED_STRING_WITH_FIRST_INIT_PASSWORD);

        Assertions.assertEquals(STRING_TO_ENCRYPT, decrypted);
    }

    @Test
    void decryptWithSecondInitPassword() {
        rc4Decryptor = new Rc4Decryptor(SECOND_INIT_PASSWORD);

        String decrypted = rc4Decryptor.decrypt(ENCRYPTED_STRING_WITH_SECOND_INIT_PASSWORD);

        Assertions.assertEquals(STRING_TO_ENCRYPT, decrypted);
    }
}