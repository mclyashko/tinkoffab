package ru.mirea.passkeeper.crypto.cryptor.rc4;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.mirea.passkeeper.crypto.Encryptor;

class Rc4EncryptorTest {

    private static final String FIRST_INIT_PASSWORD = "1234567890";

    private static final String SECOND_INIT_PASSWORD = "Lorem ipsum dolor sit amet.";

    private static final String STRING_TO_ENCRYPT = "Hello, reĝiĉo, übrigens, КПСС, ジョジョの奇妙な冒険";

    private static final String ENCRYPTED_STRING_WITH_FIRST_INIT_PASSWORD = "k5ZYhe9XW0AI8CzyzQkUonvhBLQEaBYGNHKnMNFUs85A0x4B+cVj5NP3++kDGCVpDtq7e4QAilUtb3wGyTFIOiRVshE=";

    private static final String ENCRYPTED_STRING_WITH_SECOND_INIT_PASSWORD = "Olsar8o192/cBKQXUscNLvIP8GZI5bhKerw+c84KbDQDM1ccDGOs3m8ye5/9i3vmXl00WqR2P4z9w6Kq8BPLWOpotSM=";

    Encryptor rc4Encryptor;

    @Test
    void encryptWithFirstInitPassword() {
        rc4Encryptor = new Rc4Encryptor(FIRST_INIT_PASSWORD);

        String encrypted = rc4Encryptor.encrypt(STRING_TO_ENCRYPT);

        Assertions.assertEquals(ENCRYPTED_STRING_WITH_FIRST_INIT_PASSWORD, encrypted);
    }

    @Test
    void encryptWithSecondInitPassword() {
        rc4Encryptor = new Rc4Encryptor(SECOND_INIT_PASSWORD);

        String encrypted = rc4Encryptor.encrypt(STRING_TO_ENCRYPT);

        Assertions.assertEquals(ENCRYPTED_STRING_WITH_SECOND_INIT_PASSWORD, encrypted);
    }
}