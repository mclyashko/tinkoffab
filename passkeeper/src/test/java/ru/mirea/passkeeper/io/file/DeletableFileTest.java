package ru.mirea.passkeeper.io.file;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

class DeletableFileTest {

    private static final String FILE_NAME = "test.tmp";
    @TempDir
    private static Path path;
    private static DeletableFile deletableFile;

    @BeforeEach
    void setUp() {
        Path filePath = path.resolve(FILE_NAME);

        try {
            Files.createFile(filePath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        deletableFile = new DeletableFile(filePath.toString());
    }

    @Test
    void delete() {
        deletableFile.delete();

        Assertions.assertFalse(Files.exists(path.resolve(FILE_NAME)));
    }
}