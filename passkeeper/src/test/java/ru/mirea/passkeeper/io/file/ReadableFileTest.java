package ru.mirea.passkeeper.io.file;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

class ReadableFileTest {
    private static final String FILE_NAME = "test.tmp";
    @TempDir
    private static Path path;
    private static ReadableFile readableFile;
    private static List<String> data;

    @BeforeEach
    void setUp() {
        String filePath = path.resolve(FILE_NAME).toString();

        readableFile = new ReadableFile(filePath);

        data = List.of("DATA", "TEST", "tExT");

        try {
            Files.write(Paths.get(filePath), data, StandardOpenOption.CREATE);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void read() {
        List<String> input = new ArrayList<>();

        readableFile.read(input);

        Assertions.assertEquals(data, input);
    }
}