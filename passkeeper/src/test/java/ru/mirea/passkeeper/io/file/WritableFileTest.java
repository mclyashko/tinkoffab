package ru.mirea.passkeeper.io.file;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

class WritableFileTest {

    private static final String FILE_NAME = "test.tmp";
    @TempDir
    private static Path path;
    private static WritableFile writableFile;
    private static List<String> data;

    @BeforeEach
    void setUp() {
        String filePath = path.resolve(FILE_NAME).toString();

        writableFile = new WritableFile(filePath);

        data = List.of("DATA", "TEST", "tExT");
    }

    @Test
    void write() {
        writableFile.write(data);

        List<String> input;
        try {
            input = new ArrayList<>(Files.readAllLines(path.resolve(FILE_NAME)));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Assertions.assertEquals(data, input);
    }
}