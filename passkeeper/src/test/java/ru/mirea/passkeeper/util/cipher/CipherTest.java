package ru.mirea.passkeeper.util.cipher;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.mirea.passkeeper.util.keygenerator.KeyGeneratorGetter;
import ru.mirea.passkeeper.util.securerandom.SecureRandomGetter;
import ru.mirea.passkeeper.util.string.StringUtil;

import javax.crypto.KeyGenerator;
import java.security.Key;
import java.security.SecureRandom;

class CipherTest {
    private static final String CHARSET_NAME = "UTF-8";
    private static final String SECURE_RANDOM_ALGORITHM = "SHA1PRNG";
    private static final int REAL_KEY_SIZE = 256;

    private static final String KEY_ALGORITHM = "AES";
    private static final String CIPHER_ALGORITHM_AES = "AES/ECB/PKCS5Padding";
    private static final int CIPHER_ENCRYPT_MODE = javax.crypto.Cipher.ENCRYPT_MODE;

    private static final String INIT_PASSWORD = "1234567890";

    private static final byte[] PLAIN_BYTES = StringUtil.getBytesFromString("plainText", CHARSET_NAME);

    Cipher cipher;
    javax.crypto.Cipher javaCipher;

    @SuppressWarnings("ALL")
    private Key getRealKey(String initialKey, String keyAlgorithm) {
        KeyGenerator keyGen;
        SecureRandom secureRandom;

        keyGen = KeyGeneratorGetter.getInstance(keyAlgorithm);
        secureRandom = SecureRandomGetter.getInstance(SECURE_RANDOM_ALGORITHM);

        secureRandom.setSeed(initialKey.getBytes());
        keyGen.init(REAL_KEY_SIZE, secureRandom);

        return keyGen.generateKey();
    }

    @BeforeEach
    void setUp() {
        cipher = Cipher.getInstance(CIPHER_ALGORITHM_AES);
        try {
            javaCipher = javax.crypto.Cipher.getInstance(CIPHER_ALGORITHM_AES);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        Key key = getRealKey(INIT_PASSWORD, KEY_ALGORITHM);

        cipher.init(CIPHER_ENCRYPT_MODE, key);
        try {
            javaCipher.init(CIPHER_ENCRYPT_MODE, key);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void doFinal() {
        byte[] expected;
        try {
            expected = javaCipher.doFinal(PLAIN_BYTES);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        byte[] actual =  cipher.doFinal(PLAIN_BYTES);

        Assertions.assertArrayEquals(expected, actual);
    }
}